# Proximiio Android #

Welcome to Proximiio Android! You can use this library to hook into the new Proximiio platform.

## Getting started ##

To get started, you need the library (duh). There's two ways of doing this.  
1. Download the repository (https://github.com/NavtureApps/proximiio-android-sdk-v2-dev/archive/master.zip) and place the module/directory proximiioandroidlibrary into to your Android Studio project.  
2. Alternatively, you can download the AAR file of the library (https://github.com/NavtureApps/proximiio-android-sdk-v2-dev/blob/master/proximiioandroidlibrary/build/outputs/aar/proximiioandroidlibrary-release.aar). Inside it, you can find a JAR containing the library, as well as the JAR dependencies inside the libs-directory. You should also copy the contents of res/drawable. The rest of the dependencies can be found in the build.gradle file of the library (https://github.com/NavtureApps/proximiio-android-sdk-v2-dev/blob/master/proximiioandroidlibrary/build.gradle).

## Usage ##

Create a new ProximiioListener, overriding at least the following methods.
This is an example, and you're free to use the callbacks however you wish.

    final Activity activity = this;
    ProximiioListener listener = new ProximiioListener() {
        @Override
        public void eventEnter(ProximiioInput input) {
            input.handleOutput(activity, null, true, false);
        }

        @Override
        public void eventLeave(ProximiioInput input) {
            input.handleOutput(activity, null, true, false);
        }

        @Override
        public void error(final Error error) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity, error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    };

Next, create a new instance of Proximiio, storing it in an instance variable:

    private Proximiio proximiio;
    
    ...
    
    proximiio = new Proximiio(activity, "Your App ID here!", listener);

Finally, override these methods in your activity:

    @Override
     protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        proximiio.onActivityResult(requestCode, resultCode);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        proximiio.onDestroy();
        proximiio = null;
    }

## That's it! ##

You're now fully set to use Proximiio on Android. While most of the application's settings are configured in the web portal, there's a lot more to master. Check out the rest of the methods in ProximiioListener! Full javadoc is available in the repository root.
