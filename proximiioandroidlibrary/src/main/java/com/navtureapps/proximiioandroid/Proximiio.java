package com.navtureapps.proximiioandroid;

import android.app.Activity;
import android.os.Handler;
import android.util.Log;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.MutableData;
import com.firebase.client.ServerValue;
import com.firebase.client.Transaction;
import com.firebase.client.ValueEventListener;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.indooratlas.android.sdk.IALocation;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;


/**
 * Create a new instance of this class to gain access to Proximiio Android.
 *
 * You can create multiple instances of this class, as long as they use different App IDs.
 * Creating more than one with the same App ID results in the first being removed.
 *
 * If you're shifting from background to foreground, create a new Proximiio with the same App ID as before,
 * and the background Proximiio will be merged to the instance created.
 * This allows you to create a new instance of Proximiio each time you start your app.
 */
public class Proximiio {

    private static final String TAG = "Proximiio";

    private ProximiioAPI proximiioAPI;
    private ProximiioBeaconReceiver beaconReceiver;
    private ArrayList<ProximiioInput> beaconInputs;
    private ArrayList<ProximiioInput> indoorAtlasInputs;
    private ArrayList<ProximiioInput> steerPathInputs;
    private ProximiioInput lastEvent;
    private double lastLon;
    private double lastLat;
    private double lastAccuracy;
    private boolean lastAvailable;
    private ProximiioInput.InputType lastType;
    private ProximiioFirebase proximiioFirebase;
    private boolean merged;
    private boolean ended;
    private ProximiioBeaconHelper helper;
    private String currentFloor;
    private String currentVenue;
    private Transaction.Handler increment;

    protected ProximiioIndoorAtlas indoorAtlas;
    protected ArrayList<ProximiioActionFlow> actionFlows;
    protected ArrayList<ProximiioOutput> outputs;
    protected ArrayList<ProximiioDepartment> departments;
    protected String organization;
    protected Activity activity;
    protected ProximiioID proximiioID;
    protected boolean init;
    protected boolean iBeaconsEnabled;
    protected boolean eddystoneEnabled;
    protected boolean indoorAtlasEnabled;
    protected boolean gpsGeofencesEnabled;
    protected boolean steerPathEnabled;
    protected boolean eddystoneEnabledBackground;
    protected boolean iBeaconsEnabledBackground;
    protected boolean indoorAtlasEnabledBackground;
    protected boolean gpsGeofencesEnabledBackground;
    protected boolean steerPathEnabledBackground;
    protected String apiKey;
    protected String apiSecret;
    protected String steerPathNDD;
//    protected ProximiioSteerPath steerPath;
    protected ProximiioListener listener;
    protected String appID;
    protected GeoFire organizationGeoFire;
    protected int iBeaconDuration;
    protected int iBeaconInterval;
    protected int iBeaconTimeout;
    protected int iBeaconTimeoutInterval;
    protected int iBeaconBackgroundDuration;
    protected int iBeaconBackgroundInterval;
    protected int iBeaconBackgroundTimeout;
    protected int iBeaconBackgroundTimeoutInterval;
    protected boolean background;
    protected boolean hintToIndoorAtlas;
    protected ProximiioGeofence geofence;
    protected int gpsGeofenceAccuracy;
    protected int gpsGeofenceBackgroundAccuracy;
    protected HashMap<String, Long>telemetryUpdates;

    protected static int iBeaconMinTiming = 2000; //The minimum accepted scan cycle in milliseconds, see ProximiioFirebase for deriving other values.
    protected static int iBeaconMaxTiming = 10000; //The max scan cycle.
    protected static int iBeaconMaxDuration = 2000; //Maximum scanning time, see ProximiioFirebase for usage in calculations.
    protected static int iBeaconDefault = 3000; //Default scan cycle.
    protected static int iBeaconDefaultBackground = 6000; //Default background scan cycle.
    protected static int iBeaconValid = 15000; //How often do we want position updates? (At least) this amount of time passes between position updates.
    protected static boolean defaultHint = false; //Default hintToIndoorAtlas.
    protected static int gpsGeofenceDefaultAccuracy = 3; //Default foreground native positioning accuracy, see https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest
    protected static int gpsGeofenceDefaultBackgroundAccuracy = 2; //Default background navtive positioning accuracy.

    private static ArrayList<Proximiio> instances;
    private static String tag = "Proximiio";

    /**
     * Start using Proximiio.
     * Note that you can change these parameters (authToken only supports initial set) later by calling the corresponding set-method.
     * @param activity Your application's activity.
     * @param appID Your Proximiio App ID.
     * @param authToken The auth token, as in the web portal.
     * @param newListener A ProximiioListener to receive updates.
     */
    public Proximiio(final Activity activity, final String appID, final String authToken, final ProximiioListener newListener) {
        if (appID == null || appID.equals("")) {
            this.appID = null;
        }
        else {
            this.appID = appID;
        }

        this.activity = activity;
        this.listener = newListener;
        Firebase.setAndroidContext(activity);
        background = false;
        merged = false;
        ended = false;

        boolean found = false;
        if (instances == null) {
            instances = new ArrayList<>();
        }
        else {
            for (int i = instances.size() - 1; i >= 0; i--) {
                Proximiio instance = instances.get(i);
                if (instance.appID == null || this.appID == null || instance.appID.equals(this.appID)) {
                    if (this.appID != null && instance.appID == null) {
                        setAppID(this.appID);
                    }
                    else if (this.appID == null && instance.appID != null) {
                        this.appID = instance.appID;
                    }

                    geofence = instance.geofence;
                    if (geofence != null) {
                        geofence.setProximiio(this);
                    }
                    beaconInputs = instance.beaconInputs;
                    indoorAtlasInputs = instance.indoorAtlasInputs;
//                    steerPathInputs = instance.steerPathInputs;
                    actionFlows = instance.actionFlows;
                    outputs = instance.outputs;
                    departments = instance.departments;
                    init = instance.init;
                    helper = instance.helper;
                    helper.setProximiio(this);
                    beaconReceiver = instance.beaconReceiver;
                    proximiioFirebase = instance.proximiioFirebase;
                    if (proximiioFirebase != null) {
                        proximiioFirebase.setProximiio(this);
                    }
                    else if (authToken != null && !authToken.equals("")) {
                        proximiioFirebase = new ProximiioFirebase(this, authToken);
                    }
                    organizationGeoFire = instance.organizationGeoFire;
                    lastEvent = instance.lastEvent;
                    lastLat = instance.lastLat;
                    lastLon = instance.lastLon;
                    lastAccuracy = instance.lastAccuracy;
                    lastType = instance.lastType;
                    lastAvailable = instance.lastAvailable;
                    proximiioAPI = instance.proximiioAPI;
                    proximiioID = instance.proximiioID;
                    telemetryUpdates = new HashMap<String, Long>();
                    if (proximiioID != null) {
                        proximiioID.setProximiio(this);
                    }
                    eddystoneEnabled = instance.eddystoneEnabled;
                    iBeaconsEnabled = instance.iBeaconsEnabled;
                    indoorAtlasEnabled = instance.indoorAtlasEnabled;
                    gpsGeofencesEnabled = instance.gpsGeofencesEnabled;
                    steerPathEnabled = instance.steerPathEnabled;
                    iBeaconsEnabledBackground = instance.iBeaconsEnabledBackground;
                    indoorAtlasEnabledBackground = instance.indoorAtlasEnabledBackground;
                    gpsGeofencesEnabledBackground = instance.gpsGeofencesEnabledBackground;
                    steerPathEnabledBackground = instance.steerPathEnabledBackground;
                    gpsGeofenceAccuracy = instance.gpsGeofenceAccuracy;
                    gpsGeofenceBackgroundAccuracy = instance.gpsGeofenceBackgroundAccuracy;
                    apiKey = instance.apiKey;
                    apiSecret = instance.apiSecret;
                    currentVenue = instance.currentVenue;
                    currentFloor = instance.currentFloor;
//                    steerPath = instance.steerPath;
//                    if (steerPath != null) {
//                        steerPath.setProximiio(this);
//                    }
                    indoorAtlas = instance.indoorAtlas;
                    if (indoorAtlas != null) {
                        indoorAtlas.setProximiio(this);
                        indoorAtlas.setListener(listener);
                    }
                    steerPathNDD = instance.steerPathNDD;
                    organization = instance.organization;
                    hintToIndoorAtlas = instance.hintToIndoorAtlas;
                    iBeaconDuration = instance.iBeaconDuration;
                    iBeaconInterval = instance.iBeaconInterval;
                    iBeaconTimeout = instance.iBeaconTimeout;
                    iBeaconTimeoutInterval = instance.iBeaconTimeoutInterval;
                    iBeaconBackgroundDuration = instance.iBeaconBackgroundDuration;
                    iBeaconBackgroundInterval = instance.iBeaconBackgroundInterval;
                    iBeaconBackgroundTimeout = instance.iBeaconBackgroundTimeout;
                    iBeaconBackgroundTimeoutInterval = instance.iBeaconBackgroundTimeoutInterval;

                    if (instance.background) {
                        ProximiioService.scanDuration = iBeaconDuration;
                        ProximiioService.scanInterval = iBeaconInterval;
                        ProximiioService.iBeaconTimeout = iBeaconTimeout;
                        ProximiioService.iBeaconTimeoutInterval = iBeaconTimeoutInterval;

                        if (iBeaconsEnabled != iBeaconsEnabledBackground || indoorAtlasEnabled != indoorAtlasEnabledBackground ||
                                gpsGeofencesEnabled != gpsGeofencesEnabledBackground || steerPathEnabled != steerPathEnabledBackground && eddystoneEnabled != eddystoneEnabledBackground) {
                            setUpOrganization(iBeaconsEnabled != iBeaconsEnabledBackground, eddystoneEnabled != eddystoneEnabledBackground, indoorAtlasEnabled != indoorAtlasEnabledBackground,
                                    gpsGeofencesEnabled != gpsGeofencesEnabledBackground, steerPathEnabled != steerPathEnabledBackground, true);
                        }

                        if (!iBeaconsEnabled && iBeaconsEnabledBackground || !eddystoneEnabled && eddystoneEnabledBackground) {
                            ProximiioService.destroy();
                        }

                        if (geofence != null && gpsGeofencesEnabled == gpsGeofencesEnabledBackground && gpsGeofencesEnabled && gpsGeofenceBackgroundAccuracy != gpsGeofenceAccuracy) {
                            geofence.setAccuracy(gpsGeofenceAccuracy);
                            geofence.stopUpdates();
                            geofence.startUpdates();
                        }
                    }

                    instance.merged = true;
                    setListener(listener);

                    found = true;
                    instances.remove(i);
                    break;
                }
            }
        }

        instances.add(this);

        if (!found) {
            beaconInputs = new ArrayList<>();
            indoorAtlasInputs = new ArrayList<>();
            steerPathInputs = new ArrayList<>();
            actionFlows = new ArrayList<>();
            outputs = new ArrayList<>();
            departments = new ArrayList<>();
            init = false;
            lastAvailable = false;

            geofence = new ProximiioGeofence(activity, this);

            if (authToken != null && !authToken.equals("")) {
                proximiioFirebase = new ProximiioFirebase(this, authToken);
            }

            helper = new ProximiioBeaconHelper(this);
            beaconReceiver = helper.getReceiver();
        }

        increment = new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData data) {
                if (data.getValue() == null) {
                    data.setValue(1L);
                }
                else {
                    data.setValue((long)data.getValue() + 1L);
                }
                return Transaction.success(data);
            }

            @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
            @Override
            public void onComplete(FirebaseError firebaseError, boolean b, DataSnapshot dataSnapshot) {
                if (firebaseError != null) {
                    firebaseError.toException().printStackTrace();
                }
            }
        };
    }

    public Map<String, Object>getInputs() {
        return (Map<String, Object>) proximiioFirebase.organizationData.getValue();
    }
    
    protected void resolveInputs() {
        resolveInputs(beaconInputs);
        resolveInputs(indoorAtlasInputs);
        resolveInputs(geofence.getInputs());
    }

    private void resolveInputs(ArrayList<ProximiioInput> inputs) {
        if (inputs != null && inputs.size() > 0) {
            for (int i = 0; i < inputs.size(); i++) {
                ProximiioInput input = inputs.get(i);
                input.setActionFlow(null);
                for (int j = 0; j < actionFlows.size(); j++) {
                    ProximiioActionFlow actionFlow = actionFlows.get(j);
                    if (actionFlow.getInputID().equals(input.getId())) {
                        input.setActionFlow(actionFlow);
                        break;
                    }
                }
            }
        }
    }

    protected void resolveOutputs() {
        for (int i = 0; i < actionFlows.size(); i++) {
            ProximiioActionFlow actionFlow = actionFlows.get(i);
            actionFlow.clearOutputs();
            ArrayList<String> strings = actionFlow.getOutputStrings();

            for (int j = 0; j < outputs.size(); j++) {
                ProximiioOutput output = outputs.get(j);
                for (int k = 0; k < strings.size(); k++) {
                    if (output.getID().equals(strings.get(k))) {
                        actionFlow.addOutput(output);
                    }
                }
            }
        }
    }

    /**
     * Get the last event triggered.
     * @return event if found, otherwise null.
     */
    public ProximiioInput getLastEvent() {
        if (!merged) {
            return lastEvent;
        }
        else {
            if (listener != null) {
                listener.error(ProximiioListener.Error.Merged);
            }
            return null;
        }
    }

    /**
     * Set a new listener. This will replace the old one.
     * If the listener is not null, {@link ProximiioListener#scanChange(ProximiioInput.InputType, boolean)}
     * will be called for the new listener for each input type.
     * If a position update is available, {@link ProximiioListener#positionChange(double, double, double, ProximiioInput.InputType)}
     * will also be called, representing the last position update.
     * If Indoor Atlas is enabled and fully calibrated in the background, {@link ProximiioListener#indoorAtlasAccurate()}
     * will also be called.
     *
     * @param listener A new listener.
     */
    public void setListener(ProximiioListener listener) {
        if (!merged) {
            this.listener = listener;
            if (listener != null) {
                listener.scanChange(ProximiioInput.InputType.EddyStone, !background ? eddystoneEnabled : eddystoneEnabledBackground);
                listener.scanChange(ProximiioInput.InputType.iBeacon, !background ? iBeaconsEnabled : iBeaconsEnabledBackground);
                listener.scanChange(ProximiioInput.InputType.IndoorAtlas, !background ? indoorAtlasEnabled : indoorAtlasEnabledBackground);
                listener.scanChange(ProximiioInput.InputType.GPSGeofence, !background ? gpsGeofencesEnabled : gpsGeofencesEnabledBackground);
                listener.scanChange(ProximiioInput.InputType.SteerPath, !background ? steerPathEnabled : steerPathEnabledBackground);
                listener.apiKeyChange(organization, proximiioID != null ? proximiioID.getId() : null, apiKey, apiSecret);
                if (lastAvailable) {
                    listener.positionChange(lastLat, lastLon, lastAccuracy, lastType);
                }
            }
            if (indoorAtlas != null) {
                indoorAtlas.setListener(listener);
            }
        }
        else if (this.listener != null) {
            this.listener.error(ProximiioListener.Error.Merged);
        }
    }

    /**
     * If you created Proximiio with a null authToken, use this to set it.
     * If authToken has been set and Firebase connection was succesfully established, this will have no effect.
     * If a previous authToken was invalid, you can use this to supply a new one
     * (after {@link com.navtureapps.proximiioandroid.ProximiioListener#error(ProximiioListener.Error)}, with a parameter {@link com.navtureapps.proximiioandroid.ProximiioListener.Error#InvalidToken}).
     * @param authToken The auth token, as in the web portal.
     */
    public void setAuthToken(String authToken) {
        if (!merged) {
            if (authToken != null && !authToken.equals("")) {
                if (proximiioFirebase == null) {
                    proximiioFirebase = new ProximiioFirebase(this, authToken);
                }
                else {
                    proximiioFirebase.init(authToken);
                }
            }
        }
        else if (listener != null) {
            listener.error(ProximiioListener.Error.InvalidToken);
        }
    }

    /**
     * Set a new App ID. This will replace the old one.
     * @param appID A new App ID.
     */
    public void setAppID(String appID) {
        if (!merged) {
            if (this.appID != null && proximiioFirebase != null) {
                proximiioFirebase.firebase.child("applications").child(this.appID).removeEventListener(proximiioFirebase.settingsListener);
            }

            if (appID.equals("")) {
                this.appID = null;
            }
            else {
                this.appID = appID;
            }

            init = false;
            if (this.appID != null && proximiioFirebase != null) {
                proximiioFirebase.firebase.child("applications").child(this.appID).addValueEventListener(proximiioFirebase.settingsListener);
            }
            if (organization != null) {
                cleanOrganization(organization);
            }
        }
        else if (listener != null) {
            listener.error(ProximiioListener.Error.Merged);
        }
    }

    protected void saveEddystoneTelemetryRecord(ProximiioBeacon beacon, ProximiioInput input) {
        Firebase beaconRef = proximiioFirebase.firebase.child("organizations").child(organization).child("inputMetrics").child(input.getId());
        beaconRef.child("type").setValue("eddystone-tlm");
        beaconRef.child("advertisingPDU").setValue(beacon.getPdu());
        beaconRef.child("batteryVoltage").setValue(beacon.getBatteryVoltage());
        beaconRef.child("beaconTemperature").setValue(beacon.getTemperature());
        beaconRef.child("timeUp").setValue(beacon.getUptime());
        beaconRef.child("lastUpdate").setValue(System.currentTimeMillis() / 1000L);
        long lastUpdate = System.currentTimeMillis() / 1000L;
        telemetryUpdates.put(input.getId(), lastUpdate);
    }

    protected void updateEddystoneTelemetry(ProximiioBeacon beacon) {
        if (telemetryUpdates == null) {
            telemetryUpdates = new HashMap<String, Long>();
        }
        if (organization != null) {
            ArrayList<ProximiioInput> inputs = getInput(beacon);
            if (inputs != null && inputs.size() > 0) {
                ProximiioInput input = getInput(beacon).get(0);
                if (input != null) {
                    if (!telemetryUpdates.containsKey(input.getId())) {
                        saveEddystoneTelemetryRecord(beacon, input);
                    } else {
                        long now = System.currentTimeMillis() / 1000L;
                        long lastUpdate = telemetryUpdates.get(input.getId());
                        if ((now - lastUpdate) > 300) {
                            saveEddystoneTelemetryRecord(beacon, input);
                        }
                    }
                }
            }
        }
    }

    protected void updateBeaconPositions(ArrayList<ProximiioInput> found) {
        for (int i = 0; i < found.size(); i++) {
            ProximiioInput input = found.get(i);
            ProximiioInput.InputType inputType = input.getBeacon().beaconType == ProximiioBeacon.BEACON_TYPE_IBEACON ? ProximiioInput.InputType.iBeacon : ProximiioInput.InputType.EddyStone;
            updatePositions(input.getLat(), input.getLon(), input.getAccuracy(), inputType);
        }
    }

    protected void updatePositions(double lat, double lon, double accuracy, ProximiioInput.InputType inputType) {
        Log.d(TAG, "updating positions with input:" + inputType.toString());
        if (organization != null) {
            String type = ProximiioInput.getInputTypeString(inputType);
            GeoLocation location = new GeoLocation(lat, lon);
            proximiioFirebase.geoFire.setLocation(proximiioID.getId(), location);
            organizationGeoFire.setLocation(proximiioID.getId(), location);
            updatePosition(proximiioFirebase.firebase.child("organizations").child(organization).child("anonymousVisitors").child(proximiioID.getId()).child("position"), lat, lon, accuracy, false, type);
            updatePosition(proximiioFirebase.firebase.child("organizations").child(organization).child("positions").child(proximiioID.getId()).push(), lat, lon, accuracy, true, type);

            for (int i = 0; i < departments.size(); i++) {
                ProximiioDepartment department = departments.get(i);
                if (department.contains(lat, lon, accuracy)) {
                    ArrayList<String> tags = department.getTags();
                    if (tags != null) {
                        for (int j = 0; j < tags.size(); j++) {
                            proximiioFirebase.firebase.child("organizations").child(organization).child("anonymousVisitors").child(proximiioID.getId()).child("tags").child(tags.get(j)).runTransaction(increment);
                        }
                    }
                    if (proximiioID.finishedInit()) {
                        boolean returnToDepartment = proximiioID.returningToDepartment(department.getID());
                        boolean returnToPlace = proximiioID.returningToPlace(department.getPlaceID());
                        Calendar c = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
                        c.set(Calendar.HOUR_OF_DAY, 0);
                        c.set(Calendar.MINUTE, 0);
                        c.set(Calendar.SECOND, 0);
                        c.set(Calendar.MILLISECOND, 0);
                        long date = c.getTimeInMillis();
                        Firebase placeRef = proximiioFirebase.firebase.child("organizations").child(organization).child("analytics").child(String.valueOf(date)).child(department.getPlaceID());
                        ProximiioPlace place = department.getPlace();
                        if (place != null) {
                            placeRef.child("name").setValue(place.getName());
                        }
                        placeRef.child("totalVisitors").runTransaction(increment);
                        placeRef.child(returnToPlace ? "returningVisitors" : "newVisitors").runTransaction(increment);

                        Firebase departmentRef = placeRef.child(department.getID());
                        departmentRef.child("totalVisitors").runTransaction(increment);
                        departmentRef.child(returnToDepartment ? "returningVisitors" : "newVisitors").runTransaction(increment);
                        departmentRef.child("name").setValue(department.getName());
                    }
                }
            }

            if (hintToIndoorAtlas && inputType == ProximiioInput.InputType.GPSGeofence) {
                indoorAtlas.hintPosition(lat, lon, (float)accuracy);
            }

            if (listener != null) {
                listener.positionChange(lat, lon, accuracy, inputType);
            }

            lastAvailable = true;
            lastLat = lat;
            lastLon = lon;
            lastAccuracy = accuracy;
            lastType = inputType;
        }
    }

    private void updatePosition(Firebase ref, double lat, double lon, double accuracy, boolean addID, String type) {
        ref.child("lat").setValue(lat);
        ref.child("long").setValue(lon);
        ref.child("accuracy").setValue(accuracy);
        ref.child("timestamp").setValue(ServerValue.TIMESTAMP);
        ref.child("type").setValue(type);
        if (addID) {
            ref.child("visitor").setValue(proximiioID.getId());
        }
    }

    protected ArrayList<ProximiioInput> generateBeaconEvent(ProximiioBeacon beacon, boolean enter) {
        ArrayList<ProximiioInput> found = getInput(beacon);
        for (int i = 0; i < found.size(); i++) {
            ProximiioInput input = found.get(i);
            generateEvent(input, enter);
        }
        return found;
    }

    protected Firebase generateEvent(ProximiioInput input, boolean enter) {
        if (organization != null) {
            input.setEntered(enter);

            Firebase event = proximiioFirebase.firebase.child("organizations").child(organization).child("events").push();
            Firebase root = proximiioFirebase.firebase.child("events").push();

            ProximiioDepartment department = input.getDepartment();

            if (department != null && department.getDestroyed()) {
                department = null;
            }
            if (department == null) {
                for (int i = 0; i < departments.size(); i++) {
                    ProximiioDepartment candidate = departments.get(i);
                    if (candidate.getID().equals(input.getDepartmentID())) {
                        department = candidate;
                        break;
                    }
                }
            }

            input.setDepartment(department);

            boolean needsIDAInit = false;
            String venueID = input.getVenueChange();

            if (venueID != null && department != null) {
                ProximiioPlace place = department.getPlace();
                if (place != null) {
                    if (place.getID().equals(venueID)) {
                        Log.d(TAG, "*** place: " + place.getName() +" indooratlasvenueid:" + place.getIndoorAtlasVenueID());
                        needsIDAInit = currentVenue == null || !currentVenue.equals(place.getIndoorAtlasVenueID());
                        currentVenue = place.getIndoorAtlasVenueID();
                    }
                    else {
                        Log.w(tag, "Invalid place on IDA venue change!");
                    }
                }
                else {
                    Log.w(tag, "Triggered an IDA venue change, but place was null!");
                }
            }
            String floorID = input.getFloorChange();

            if (floorID != null) {
                needsIDAInit = currentFloor == null || !currentFloor.equals(floorID);
                currentFloor = floorID;
            }
            if (needsIDAInit) {
                setUpOrganization(false, false, true, false, false, true);
            }

            createEvent(event, input, department, enter, false);
            createEvent(root, input, department, enter, true);

            if (department != null) {
                updateVisitors(input, enter, "departments", input.getDepartmentID(), true);
                updateVisitors(input, enter, "places", department.getPlaceID(), false);
            }

            if (proximiioID.finishedInit() && department != null) {
                Calendar c = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);
                long date = c.getTimeInMillis();
                Firebase placeRef = proximiioFirebase.firebase.child("organizations").child(organization).child("analytics").child(String.valueOf(date)).child(department.getPlaceID());
                ProximiioPlace place = department.getPlace();
                if (place != null) {
                    placeRef.child("name").setValue(place.getName());
                }
                placeRef.child("events").runTransaction(increment);

                Firebase departmentRef = placeRef.child(department.getID());
                departmentRef.child("events").runTransaction(increment);
                departmentRef.child("name").setValue(department.getName());
            }

            if (listener != null) {
                if (enter) {
                    listener.eventEnter(input);
                }
                else {
                    listener.eventLeave(input);
                }
            }

            lastEvent = input;

            return event;
        }  else {
            return null;
        }
    }

    private void createEvent(Firebase event, ProximiioInput input, ProximiioDepartment department, boolean enter, boolean root) {
        ProximiioActionFlow actionFlow = input.getActionFlow();

        event.child("timestamp").setValue(ServerValue.TIMESTAMP);
        event.child("visitor").setValue(proximiioID.getId());
        event.child("type").setValue(ProximiioInput.getInputTypeString(input.getType()));

        if (root) {
            event.child("organization").setValue(organization);
        }

        Firebase data = event.child("data");
        if (actionFlow != null) {
            data.child("actionflow").setValue(actionFlow.getID());
        }

        if (input.getType() == ProximiioInput.InputType.iBeacon) {
            data.child("uuid").setValue(input.getBeacon().getUUID());
            data.child("major").setValue(input.getBeacon().getMajor());
            data.child("minor").setValue(input.getBeacon().getMinor());
        }

        if (input.getType() == ProximiioInput.InputType.EddyStone) {
            data.child("uuid").setValue(input.getBeacon().getUUID());
            data.child("namespace").setValue(input.getBeacon().getNamespace());
            data.child("instanceId").setValue(input.getBeacon().getInstanceId());
        }

        data.child("lat").setValue(input.getLat());
        data.child("long").setValue(input.getLon());
        data.child("accuracy").setValue(input.getAccuracy());
        data.child("flow").setValue(enter ? "enter" : "leave");
        data.child("inputID").setValue(input.getId());
        data.child("departmentID").setValue(input.getDepartmentID());
        if (department != null) {
            data.child("department").setValue(department.getName());
            data.child("placeID").setValue(department.getPlaceID());
            ProximiioPlace place = department.getPlace();
            if (place != null) {
                data.child("place").setValue(place.getName());
            }
        }
    }

    private void updateVisitors(final ProximiioInput input, final boolean enter, String typeString, String typeID, final boolean department) {
        final Firebase visitors = proximiioFirebase.firebase.child("organizations").child(organization).child("visitors").child(typeString).child(typeID);

        if (enter) {
            if (department) {
                Firebase visitor = visitors.child("visitors").push();
                input.setVisitorID(visitor.getKey());
                visitor.child("timestamp").setValue(ServerValue.TIMESTAMP);
            }
            else {
                visitors.child("visitors").child(input.getVisitorID()).child("timestamp").setValue(ServerValue.TIMESTAMP);
            }
        }

        visitors.addListenerForSingleValueEvent(new ValueEventListener() {
            @SuppressWarnings("unchecked")
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int visitorCount = 0;
                long time = System.currentTimeMillis();
                Map<String, Object> data = (Map<String, Object>) dataSnapshot.getValue();
                Map<String, Object> value = (Map<String, Object>) data.get("visitors");
                if (value != null) {
                    for (Map.Entry<String, Object> entry : value.entrySet()) {
                        Map<String, Object> next = (Map<String, Object>) entry.getValue();

                        //If this entry matches this leave event or is more than 1 hour old, remove it
                        if ((!enter && entry.getKey().equals(input.getVisitorID())) || (time - (long) next.get("timestamp")) / 3600000 > 1) {
                            visitors.child("visitors").child(entry.getKey()).removeValue();
                        } else {
                            visitorCount++;
                        }
                    }
                }

                visitors.child("totalVisitors").setValue(visitorCount);
            }

            @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
            @Override
            public void onCancelled(FirebaseError firebaseError) {
                firebaseError.toException().printStackTrace();
            }
        });
    }

    protected ArrayList<ProximiioInput> getInput(ProximiioBeacon beacon) {
        ArrayList<ProximiioInput> found = new ArrayList<>();
        for (int i = 0; i < beaconInputs.size(); i++) {
            ProximiioInput input = beaconInputs.get(i);
            if (input.getBeacon() != null) {
                if (input.getBeacon().equals(beacon)) {
                    input.setAccuracy(beacon.getAccuracy());
                    found.add(input);
                }
            }
        }
        return found;
    }

    //Get the organization's inputs and use them as filters for the iBeacon scanning and Indoor Atlas, updated in real time
    @SuppressWarnings({"unchecked", "ConstantConditions"})
    protected void setUpOrganization(boolean iBeaconsReset, boolean eddystoneReset, boolean indoorAtlasReset, boolean gpsGeofenceReset, boolean steerPathReset, boolean clean) {
        if (init && proximiioFirebase.organizationData != null && proximiioFirebase.organizationData.getValue() != null) {
            if (iBeaconsReset) {
                if (clean) {
                    cleanConnections(true, false, false, false);
                }
                beaconReceiver.removeAllRegions();
                beaconInputs.clear();
            }
            if (eddystoneReset) {
                if (clean) {
                    cleanConnections(false, true, false, false);
                }
                beaconReceiver.removeAllRegions();
                beaconInputs.clear();
            }
            if (indoorAtlasReset) {
                if (clean) {
                    cleanConnections(false, false, true, false);
                }
                indoorAtlasInputs.clear();
            }
            if (gpsGeofenceReset) {
                geofence.removeGeofences();
                geofence.setAccuracy(background ? gpsGeofenceBackgroundAccuracy : gpsGeofenceAccuracy);
            }
            if (steerPathReset) {
                if (clean) {
                    cleanConnections(false, false, false, true);
                }
//                steerPathInputs.clear();
            }
            boolean iBeaconsAvailable = false;
            boolean eddystoneAvailable = false;
            boolean indoorAtlasAvailable = false;
            boolean gpsGeofencesAvailable = false;
            boolean steerPathAvailable = false;
            Map<String, Object> value = (Map<String, Object>) proximiioFirebase.organizationData.getValue();
            for (Map.Entry<String, Object> entry : value.entrySet()) {
                Map<String, Object> next = (Map<String, Object>) entry.getValue();
                String type = (String) next.get("type");
                Map<String, Object> data = (Map<String, Object>) next.get("data");
                if (type != null && data != null) {
                    Map<String, Object> map = (Map<String, Object>) data.get("map");
                    Map<String, Object> marker = (Map<String, Object>) data.get("marker");
                    Map<String, Object> circle = (Map<String, Object>) data.get("circle");
                    ProximiioInput input = null;
                    Log.d(TAG, "indooratlasreset:" + indoorAtlasReset + " typeIsIDA:" + type.equals(ProximiioInput.getInputTypeString(ProximiioInput.InputType.IndoorAtlas)) + " background:" + background + " indoorAtlasEnabled:" + indoorAtlasEnabled + " enabledBG:" + indoorAtlasEnabledBackground);
                    if (iBeaconsReset && (type.equals(ProximiioInput.getInputTypeString(ProximiioInput.InputType.iBeacon))) && ((!background && iBeaconsEnabled) || (background && iBeaconsEnabledBackground))) {
                        ProximiioBeacon beacon = new ProximiioBeacon((String) data.get("uuid"), (int) (long) data.get("major"), (int) (long) data.get("minor"));
                        iBeaconsAvailable = true;

                        input = new ProximiioInput(beacon, (double) marker.get("lat"), (double) marker.get("lng"), entry.getKey(), (String)next.get("departmentUID"), (String)next.get("name"));

                        beaconInputs.add(input);
                        beaconReceiver.addRegion(new ProximiioRegion(beacon));
                    }
                    else if (eddystoneReset && (type.equals(ProximiioInput.getInputTypeString(ProximiioInput.InputType.EddyStone))) && (!background && eddystoneEnabled) || (background && eddystoneEnabledBackground)) {
                        ProximiioBeacon beacon = new ProximiioBeacon((String) data.get("namespaceid"), (String)data.get("instanceid"));
                        eddystoneAvailable = true;
                        Log.d(TAG, "adding input marker:" + marker);
                        if (marker != null) {
                            input = new ProximiioInput(beacon, (double) marker.get("lat"), (double) marker.get("lng"), entry.getKey(), (String) next.get("departmentUID"), (String) next.get("name"));
                            beaconInputs.add(input);
                            beaconReceiver.addRegion(new ProximiioRegion(beacon));
                        }

                    }
                    else if (indoorAtlasReset && type.equals(ProximiioInput.getInputTypeString(ProximiioInput.InputType.IndoorAtlas)) && ((!background && indoorAtlasEnabled) || (background && indoorAtlasEnabledBackground))) {
                        Log.d(TAG, "IDA INIT");
                        double radius;
                        try {
                            radius = (double)(long) data.get("radius");
                        } catch (ClassCastException e) {
                            radius = (double) data.get("radius");
                        }
                        double lat = 0;
                        double lng = 0;

                        if (marker != null) {
                            lat = (double)marker.get("lat");
                            lng = (double)marker.get("lng");
                        } else {
                            if (circle != null) {
                                lat = (double)circle.get("lat");
                                lng = (double)circle.get("lng");
                            }
                        }

                        input = new ProximiioInput(lat, lng, radius, ProximiioInput.InputType.IndoorAtlas, entry.getKey(), (String)next.get("departmentUID"), (String)next.get("name"));
                        indoorAtlasInputs.add(input);
                        Log.d(TAG, "adding indooratlas input:" + input.getName());
                        indoorAtlasAvailable = true;
                    }
                    else if (gpsGeofenceReset && type.equals(ProximiioInput.getInputTypeString(ProximiioInput.InputType.GPSGeofence)) && ((!background && gpsGeofencesEnabled) || (background && gpsGeofencesEnabledBackground))) {
                        double radius;
                        try {
                            radius = (double)(long) data.get("radius");
                        } catch (ClassCastException e) {
                            radius = (double) data.get("radius");
                        }
                        double lat = 0;
                        double lng = 0;

                        if (marker != null) {
                            lat = (double)marker.get("lat");
                            lng = (double)marker.get("lng");
                        } else {
                            if (circle != null) {
                                lat = (double)circle.get("lat");
                                lng = (double)circle.get("lng");
                            }
                        }

                        gpsGeofencesAvailable = true;
                        input = new ProximiioInput(lat, lng, radius, ProximiioInput.InputType.GPSGeofence, entry.getKey(), (String)next.get("departmentUID"), (String)next.get("name"));
                        geofence.addGeofence(input);
                    }
                    else if (steerPathReset && type.equals(ProximiioInput.getInputTypeString(ProximiioInput.InputType.SteerPath)) && ((!background && steerPathEnabled) || (background && steerPathEnabledBackground))) {
//                        double radius;
//                        try {
//                            radius = (double)(long) data.get("radius");
//                        } catch (ClassCastException e) {
//                            radius = (double) data.get("radius");
//                        }
//                        double lat = 0;
//                        double lng = 0;
//
//                        if (marker != null) {
//                            lat = (double)marker.get("lat");
//                            lng = (double)marker.get("lng");
//                        } else {
//                            if (circle != null) {
//                                lat = (double)circle.get("lat");
//                                lng = (double)circle.get("lng");
//                            }
//                        }
//                        input = new ProximiioInput(lat, lng, radius, ProximiioInput.InputType.SteerPath, entry.getKey(), (String)next.get("departmentUID"), (String)next.get("name"));
//                        steerPathInputs.add(input);
//                        steerPathAvailable = true;
                    }
                    if (input != null) {
                        Object triggerVenueChange = next.get("triggerVenueChange");
                        if (triggerVenueChange != null && (boolean)triggerVenueChange) {
                            input.setVenueChange((String)next.get("placeUID"));
                        }
                        Object triggerFloorChange = next.get("triggerFloorChange");
                        if (triggerFloorChange != null && (boolean)triggerFloorChange) {
                            input.setFloorChange((String)next.get("floorUID"));
                        }
                    }
                }
            }

            if (iBeaconsReset) {
                resolveInputs(beaconInputs);
                if (iBeaconsAvailable) {
                    toast("iBeacon scan started!");
                    initBeacons();
                }
                else if ((!background && iBeaconsEnabled) || (background && iBeaconsEnabledBackground)) {
                    toast("iBeacons set to true, but no iBeacon inputs! Scan not started.");
                }
            }

            if (eddystoneReset) {
                resolveInputs(beaconInputs);
                if (eddystoneAvailable) {
                    toast("Eddystone scan started!");
                    initBeacons();
                }
                else if ((!background && eddystoneEnabled) || (background && eddystoneEnabledBackground)) {
                    toast("Eddystone set to true, but no Eddystone inputs! Scan not started.");
                }
            }

            if (indoorAtlasReset) {
                resolveInputs(indoorAtlasInputs);
                if (indoorAtlasAvailable) {
                    toast("IndoorAtlas scan started!");
                    initIndoorAtlas();
                }
                else if ((!background && indoorAtlasEnabled) || (background && indoorAtlasEnabledBackground)) {
                    toast("IndoorAtlas set to true, but no IndoorAtlas inputs! Positioning enabled.");
                    initIndoorAtlas();
                }
            }

            if (gpsGeofenceReset) {
                resolveInputs(geofence.getInputs());
                if (gpsGeofencesAvailable) {
                    toast("GPS Geofencing started!");
                }
                else if (((!background && gpsGeofencesEnabled) || (background && gpsGeofencesEnabledBackground))) {
                    toast("GPS Geofences set to true, but no GPS Geofence inputs! Location updates on.");
                    geofence.startUpdates();
                }
                else {
                    geofence.stopUpdates();
                }
            }

//            if (steerPathReset) {
//                resolveInputs(steerPathInputs);
//                if (steerPathAvailable) {
////                    toast("SteerPath scan started!");
////                    initSteerPath();
//                }
//                else if (((!background && steerPathEnabled) || (background && steerPathEnabledBackground))) {
////                    toast("SteerPath set to true, but no SteerPath inputs! SteerPath scan on.");
////                    initSteerPath();
//                }
//            }
        }
    }

    private void initBeacons() {
        if (proximiioAPI == null) {
            ProximiioService.iBeaconValid = iBeaconValid;
            proximiioAPI = new ProximiioAPI(activity, true, beaconReceiver, this);
        }
    }

    protected void initIndoorAtlas() {
        Log.d(TAG, "initIndoorAtlas");
        Log.d(TAG, "indoorAtlas:" + (indoorAtlas == null ? "not initialized yet" : indoorAtlas.toString()));
        Log.d(TAG, "indoorAtlas ready:" + proximiioFirebase.indoorAtlasReady);
        if (indoorAtlas == null && proximiioFirebase.indoorAtlasReady) {
            Log.d(TAG, "proximiioFloor floors:" + currentFloor);
            ProximiioFloor floor = proximiioFirebase.floors.get(currentFloor != null ? currentFloor : proximiioFirebase.defaultFloor);
            Log.d(TAG, "proximiiofloor current:" + proximiioFirebase.floors.get(currentFloor));
            Log.d(TAG, "proximiiofloor default:" + proximiioFirebase.floors.get(proximiioFirebase.defaultFloor));
            Log.d(TAG, "proximiioFloor: " + floor);
            Log.d(TAG, "proximiioCurrentFloor: " + currentFloor);
            Log.d(TAG, "proximiioCurrentVenue: " + currentVenue);
            if (floor != null && floor.getFloorId() != null && !floor.getFloorId().equals("") && floor.getFloorPlanId() != null && !floor.getFloorPlanId().equals("")) {
                indoorAtlas = new ProximiioIndoorAtlas(activity, this, listener, apiKey, apiSecret, currentVenue, floor.getFloorId(), floor.getFloorPlanId());
                Log.d(TAG, "proximiiofloor: floorId:" + floor.getFloorId() + "" + " floorplanId:" + floor.getFloorPlanId() + " currentVenue:" + (currentVenue == null ? "currentVenue null" : currentVenue));
            }
            else {
                Log.w(tag, "No Indoor Atlas floors and/or venue found!");
            }
        }
        proximiioFirebase.indoorAtlasReady = true;
    }

//    private void initSteerPath() {
//        if (steerPath == null) {
//            steerPath = new ProximiioSteerPath(activity, steerPathNDD, this);
//        }
//    }

    protected void indoorAtlasInput(IALocation location, boolean forceLeave) {
        double lat = location.getLatitude();
        double lon = location.getLongitude();
        for (int i = 0; i < indoorAtlasInputs.size(); i++) {
            ProximiioInput input = indoorAtlasInputs.get(i);
            if (!forceLeave) {
                if (input.entered(lat, lon)) {
                    input.setAccuracy(location.getAccuracy());
                    generateEvent(input, true);
                }
                else if (input.left(lat, lon)) {
                    input.setAccuracy(location.getAccuracy());
                    generateEvent(input, false);
                }
            }
            else if (input.getEntered()) {
                input.setAccuracy(location.getAccuracy());
                generateEvent(input, false);
            }
        }
    }

    protected void steerPathInput(double lat, double lon, double accuracy) {
        for (int i = 0; i < steerPathInputs.size(); i++) {
            ProximiioInput input = steerPathInputs.get(i);
            if (input.entered(lat, lon)) {
                input.setAccuracy(accuracy);
                generateEvent(input, true);
            }
            else if (input.left(lat, lon)) {
                input.setAccuracy(accuracy);
                generateEvent(input, false);
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode) {
        if (proximiioAPI != null) {
            proximiioAPI.activityResult(requestCode, resultCode);
        }
    }

    protected void toast(final String message) {
        Log.d(tag, message);
        /*activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
            }
        });*/
    }

    /**
     * Call this on your Activity's onStop method if you want to stop Proximiio.
     */
    public void onDestroy() {
        if (!merged) {
            init = false;
            if (organization != null) {
                cleanOrganization(organization);
            }
            if (appID != null && proximiioFirebase != null) {
                proximiioFirebase.firebase.child("applications").child(appID).removeEventListener(proximiioFirebase.settingsListener);
            }
            cleanConnections(true, true, true, true);
            if (ProximiioService.enabled()) {
                ProximiioService.destroy();
            }
            if (geofence != null) {
                geofence.destroy(false);
                geofence = null;
            }

            if (proximiioFirebase != null) {
                proximiioFirebase.firebase.unauth();
            }

            merged = true;
        }
        else if (listener != null) {
            listener.error(ProximiioListener.Error.Merged);
        }

        instances.remove(this);
    }

    /**
     * Call this on your Activity's onStop method if you don't want to stop Proximiio.
     * If the new obscuring Activity doesn't create a new Proximiio in its onStart with the same App ID,
     * this Proximiio will be moved to background and background specific timings will be applied.
     */
    public void stopActivity() {
        if (!ended && !merged) {
            if (geofence != null) {
                geofence.destroy(true);
            }

            merged = false;

            ProximiioService.scanDuration = iBeaconBackgroundDuration;
            ProximiioService.scanInterval = iBeaconBackgroundInterval;
            ProximiioService.iBeaconTimeout = iBeaconBackgroundTimeout;
            ProximiioService.iBeaconTimeoutInterval = iBeaconBackgroundTimeoutInterval;
            background = true;

            if (iBeaconsEnabled != iBeaconsEnabledBackground || eddystoneEnabled != eddystoneEnabledBackground || indoorAtlasEnabled != indoorAtlasEnabledBackground ||
                    gpsGeofencesEnabled != gpsGeofencesEnabledBackground || steerPathEnabled != steerPathEnabledBackground) {
                setUpOrganization(iBeaconsEnabled != iBeaconsEnabledBackground, eddystoneEnabled != eddystoneEnabledBackground, indoorAtlasEnabled != indoorAtlasEnabledBackground,
                        gpsGeofencesEnabled != gpsGeofencesEnabledBackground, steerPathEnabled != steerPathEnabledBackground, true);
            }

            if (proximiioAPI != null) {
                try {
                    proximiioAPI.destroy();
                }
                catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
            }

            if (geofence != null && gpsGeofencesEnabled == gpsGeofencesEnabledBackground && gpsGeofencesEnabledBackground && gpsGeofenceBackgroundAccuracy != gpsGeofenceAccuracy) {
                geofence.setAccuracy(gpsGeofenceBackgroundAccuracy);
                geofence.stopUpdates();
                geofence.startUpdates();
            }

            if (listener != null) {
                if (iBeaconsEnabled != iBeaconsEnabledBackground) {
                    listener.scanChange(ProximiioInput.InputType.iBeacon, iBeaconsEnabledBackground);
                }
                if (eddystoneEnabled != eddystoneEnabledBackground) {
                    listener.scanChange(ProximiioInput.InputType.EddyStone, eddystoneEnabledBackground);
                }
                if (indoorAtlasEnabled != indoorAtlasEnabledBackground) {
                    listener.scanChange(ProximiioInput.InputType.IndoorAtlas, indoorAtlasEnabledBackground);
                }
                if (gpsGeofencesEnabled != gpsGeofencesEnabledBackground) {
                    listener.scanChange(ProximiioInput.InputType.GPSGeofence, gpsGeofencesEnabledBackground);
                }
                if (steerPathEnabled != steerPathEnabledBackground) {
                    listener.scanChange(ProximiioInput.InputType.SteerPath, steerPathEnabledBackground);
                }
            }

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (listener != null) {
                        listener.safeBackground();
                    }
                }
            }, 50);

            ended = true;
        }
    }

    protected void cleanConnections(boolean iBeaconClean, boolean eddystoneClean, boolean indoorAtlasClean, boolean steerPathClean) {
        if (iBeaconClean && proximiioAPI != null) {
            for (int i = 0; i < beaconInputs.size(); i++) {
                ProximiioInput input = beaconInputs.get(i);
                if (input.getEntered()) {
                    generateEvent(input, false);
                }
            }
            proximiioAPI.runOnBackground(false);
            try {
                proximiioAPI.destroy();
            }
            catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
            proximiioAPI = null;
        }
        if (indoorAtlasClean && indoorAtlas != null) {
            indoorAtlas.destroy();
            indoorAtlas = null;
        }
//        if (steerPathClean && steerPath != null) {
//            for (int i = 0; i < steerPathInputs.size(); i++) {
//                ProximiioInput input = steerPathInputs.get(i);
//                if (input.getEntered()) {
//                    generateEvent(input, false);
//                }
//            }
//            steerPath.destroy();
//            steerPath = null;
//        }
    }

    protected void cleanOrganization(String id) {
        if (proximiioFirebase != null) {
            proximiioFirebase.firebase.child("organizations").child(id).child("inputs").removeEventListener(proximiioFirebase.organizationListener);
            proximiioFirebase.firebase.child("organizations").child(id).child("actionflows").removeEventListener(proximiioFirebase.actionFlowListener);
            proximiioFirebase.firebase.child("organizations").child(id).child("outputs").removeEventListener(proximiioFirebase.outputListener);
            proximiioFirebase.firebase.child("organizations").child(id).child("departments").removeEventListener(proximiioFirebase.departmentListener);
            proximiioFirebase.firebase.child("organizations").child(id).child("floors").removeEventListener(proximiioFirebase.floorListener);
            proximiioFirebase.firebase.child("organizations").child(id).child("places").removeEventListener(proximiioFirebase.placeListener);
        }

        for (int i = departments.size() - 1; i >= 0; i--) {
            departments.get(i).destroy(true);
            departments.remove(i);
        }
    }
}
