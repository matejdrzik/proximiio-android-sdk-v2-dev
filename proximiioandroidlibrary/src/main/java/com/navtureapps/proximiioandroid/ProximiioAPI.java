//Copyright � 2014 NavtureApps (http://navtureapps.com/)

package com.navtureapps.proximiioandroid;

import java.util.ArrayList;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.ErrorDialogFragment;
import com.google.android.gms.common.GooglePlayServicesUtil;

import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

public class ProximiioAPI {
	private static final String TAG = "ProximiioAPI";
	private Activity activity;
	private BluetoothAdapter bta;
	private Messenger beaconMessenger;
	private boolean isIBeaconBound;
	private ServiceConnection iBeaconConnection;
	private boolean isGeofenceBound;
	private ProximiioBeaconReceiver receiver;
	private boolean background;
	private ArrayList<Message> iBeaconQueue;
	private ArrayList<Message> eddystoneQueue;
	private ProximiioConfig config;
	private ArrayList<ProximiioGeofenceReceiver> geofenceReceivers;
	private Proximiio proximiioInstance;
	private static String apiKey;
	private static boolean pushNotifications;
	private static ProximiioBeaconReceiver proximiioBeaconReceiver;

	private static ProximiioNetwork proximiioNetwork;
	private static ProximiioNetReceiver proximiioNetReceiver;
	private static ProximiioGeofencing proximiioGeofencing;
	
	private static boolean requesting = false;
	private static boolean checkedGooglePlay = false;
	private static boolean hasGooglePlay = false;
	private static String tag = "ProximiioAPI";
	
	public static final int BLUETOOTH_REQUEST = 776946446;
	public static final int GOOGLE_PLAY_REQUEST = 776946447;
	public static final String PROXIMIIO_UUID = "2E1A0B14-BFAB-40D7-88F8-993BC447BE06";
	
	/**
	 * Hook up to the Proximiio API.
	 * 
	 * @param activity The activity of your application.
	 * @param runOnBackground Should the Bluetooth scan service run on the background after the activity has finished?
	 * @param receiver A listener that will be provided with the info from Bluetooth scans.
	 */
	public ProximiioAPI(final Activity activity, boolean runOnBackground, final ProximiioBeaconReceiver receiver, Proximiio proximiio) {
		this(activity, runOnBackground, null, receiver, null);
		this.proximiioInstance = proximiio;
	}

	/**
	 * Hook up to the Proximiio API and use your Proximiio API-key to send data to the cloud.
	 * @param activity The activity of your application.
	 * @param runOnBackground Should the Bluetooth scan service run on the background after the activity has finished?
	 * @param apiKey Your Proximiio API-key.
	 * @param receiver A listener that will be provided with the info from Bluetooth scans.
	 * @param netReceiver A ProximiioNetReceiver that will be informed about incoming data from the cloud.
	 */
	public ProximiioAPI(final Activity activity, boolean runOnBackground, final String apiKey, final ProximiioBeaconReceiver receiver, ProximiioNetReceiver netReceiver) {
		background = runOnBackground;
		this.receiver = receiver;

		iBeaconQueue = new ArrayList<Message>();

		iBeaconConnection = new ServiceConnection() {
		    public void onServiceConnected(ComponentName className, IBinder service) {
		        beaconMessenger = new Messenger(service);

		        sendMessage(beaconMessenger, ProximiioService.REGISTER_CLIENT, receiver);

		        if (apiKey != null) {
		        	sendMessage(beaconMessenger, ProximiioService.REGISTER_CLIENT, proximiioBeaconReceiver);
		        }

		        for (int i = 0; i < iBeaconQueue.size(); i++) {
		        	sendMessage(beaconMessenger, iBeaconQueue.get(i));
		        }

		        iBeaconQueue.clear();
		    }

		    //Service crasPhed
		    public void onServiceDisconnected(ComponentName className) {
		        beaconMessenger = null;
		    }
		};

		this.activity = activity;
		geofenceReceivers = new ArrayList<ProximiioGeofenceReceiver>();
		bta = BluetoothAdapter.getDefaultAdapter();
		if (!requesting) {
			if (!bta.isEnabled()) {
				requesting = true;
			    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			    activity.startActivityForResult(enableBtIntent, BLUETOOTH_REQUEST);
			}
			else {
				service();
			}
		}

        checkGooglePlay();

		if (hasGooglePlay) {
			geofenceService();
		}
	}
	
	private void geofenceService() {
		if (!isGeofenceBound) {
			isGeofenceBound = true;
		}
	}
	
	private void startGeofencing() {
		if (checkedGooglePlay) {
			if (hasGooglePlay) {
				Log.d(tag, "Google Play Services OK!");
				if (proximiioNetwork != null) {
					if (proximiioGeofencing == null) {
						proximiioGeofencing = new ProximiioGeofencing(activity);
						IntentFilter filter = new IntentFilter();
						filter.addAction("com.aol.android.geofence.ACTION_RECEIVE_GEOFENCE");
						activity.registerReceiver(proximiioGeofencing, filter);
					}
					proximiioNetwork.getGeofences(proximiioGeofencing);
					geofenceService();
				}
			}
			else {
				Log.w(tag, "Google Play Services unavailable!");
			}
		}
	}
	
	private void checkGooglePlay() {
		if (!checkedGooglePlay) {
			int result = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
			if (result == ConnectionResult.SUCCESS) {
				checkedGooglePlay = true;
				hasGooglePlay = true;
				startGeofencing();
			}
			else {
				Dialog dialog = GooglePlayServicesUtil.getErrorDialog(result, activity, GOOGLE_PLAY_REQUEST);
				ErrorDialogFragment errorFragment = ErrorDialogFragment.newInstance(dialog);
				errorFragment.show(activity.getFragmentManager(), "ProximiioAPI Play Services Request");
			}
		}
	}
	
	/**
	 * A function to call onActivityResult to enable Bluetooth status-checking.
	 * <br/>
	 * You should add a function to your activity class as follows:
	 * 
	 * <pre>{@code {@literal @}Override
	 * protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	 *     proximiioAPI.ActivityResult(requestCode, resultCode);
	 * }</pre>
	 * 
	 * This will allow the API to ask the user to turn Bluetooth on.
	 * If you have multiple instances of ProximiioAPI, call this for all of them.
	 * 
	 * @param requestCode The request code that was submitted, pass on from onActivityResult.
	 * @param resultCode The result code that was returned, pass on from onActivityResult.
	 */
	public void activityResult(int requestCode, int resultCode) {
		switch (requestCode) {
		case BLUETOOTH_REQUEST:
			if (resultCode == Activity.RESULT_CANCELED) {
				Toast.makeText(activity, "Bluetooth needs to be enabled. Please restart the application and enable Bluetooth.", Toast.LENGTH_SHORT).show();
			}
			else if (resultCode == Activity.RESULT_OK) {
				service();
			}
			break;
		case GOOGLE_PLAY_REQUEST:
			checkedGooglePlay = true;
			hasGooglePlay = requestCode == Activity.RESULT_OK;
			startGeofencing();
		}
	}
	
	/**
	 * Call this on your activity's onDestroy() to clean up service connections.
	 */
	public void destroy() {
		if (isIBeaconBound && beaconMessenger != null) {
        	if (!background) {
        		sendMessage(beaconMessenger, ProximiioService.UNREGISTER_CLIENT, receiver);
        		if (apiKey != null) {
        			sendMessage(beaconMessenger, ProximiioService.UNREGISTER_CLIENT, proximiioBeaconReceiver);
        		}
        	}
            activity.unbindService(iBeaconConnection);
            isIBeaconBound = false;
        }
		if (isGeofenceBound) {
			for (int i = 0; i < geofenceReceivers.size(); i++) {
				proximiioGeofencing.removeReceiver(geofenceReceivers.get(i));
			}
			activity.unregisterReceiver(proximiioGeofencing);
			proximiioGeofencing = null;
			isGeofenceBound = false;
		}
		if (config != null) {
			config.destroy();
			config = null;
		}
		checkedGooglePlay = false;
		if (proximiioGeofencing != null) {
			proximiioGeofencing.destroy();
		}
	}

	/**
	 * Add a new geofencing receiver to receive information about geofences' events.
	 * @param receiver A ProximiioGeofenceReceiver
	 */
	public void addGeofenceReceiver(ProximiioGeofenceReceiver receiver) {
		if (proximiioGeofencing != null) {
			geofenceReceivers.add(receiver);
			proximiioGeofencing.addReceiver(receiver);
		}
		else {
			Log.e(tag, "No geofencing available!");
		}
	}
	
	public void removeGeofenceReceiver(ProximiioGeofenceReceiver receiver) {
		if (proximiioGeofencing != null) {
			geofenceReceivers.remove(receiver);
			proximiioGeofencing.removeReceiver(receiver);
		}
		else {
			Log.e(tag, "No geofencing available!");
		}
	}
	
	/**
	 * Automatically create notifications from incoming data.
	 * @param enable Whether to enable this or not.
	 */
	public void enablePushNotifications(boolean enable) {
		pushNotifications = enable;
	}
	
	/**
	 * Returns the current state of push notifications.
	 * @return True for enabled, false for disabled.
	 */
	public boolean pushNotificationsEnabled() {
		return pushNotifications;
	}
	
	private void service() {
		if (!ProximiioService.enabled()) {
			activity.startService(new Intent(activity, ProximiioService.class));
		}
		activity.bindService(new Intent(activity, ProximiioService.class), iBeaconConnection, Context.BIND_AUTO_CREATE);
		isIBeaconBound = true;
	}
	
	private void sendMessage(Messenger messenger, int type, Object obj) {
		sendMessage(messenger, Message.obtain(null, type, obj));
	}
	
	private void sendMessage(Messenger messenger, Message message) {
		try {
			if (messenger != null) {
				messenger.send(message);
			}
			else {
				queueMessage(messenger, message);
			}
		}
		catch (RemoteException e) {
			queueMessage(messenger, message);
		}
	}
	
	private void queueMessage(Messenger messenger, Message message) {
		if (messenger == beaconMessenger) {
			iBeaconQueue.add(message);
		}
	}
	
	/**
	 * Change Bluetooth scan service background behaviour.
	 * @param enable Do you want the Bluetooth scan service to run on the background after the activity has finished?
	 */
	public void runOnBackground(boolean enable) {
		background = enable;
	}
	
	/**
	 * Stop the background Bluetooth scanning.
	 * This will cause lostIBeacon to fire on all iBeacons, with the reason SCAN_STOP.
	 * The advantage to using this instead of ProximiioService.destroy() is that all instances of ProximiioAPI will remain usable,
	 * and all the registered receivers are used automatically when calling startScanning().
	 */
	public void stopScanning() {
		sendMessage(beaconMessenger, ProximiioService.STOP_SCAN, null);
	}
	
	/**
	 * Start background Bluetooth scanning.
	 * This doesn't need to be called after instance creation, since scanning will be automatically started.
	 * Note that scanning will also start in the case that a new instance is created.
	 */
	public void startScanning() {
		sendMessage(beaconMessenger, ProximiioService.START_SCAN, null);
	}
	
	/**
	 * Use this to start configuring your Proximiio-supplied iBeacons.
	 * Please note that iBeacons can only be configured one at a time.
	 * Configuring an iBeacon will drop all iBeacons from the scanning.
	 * @param beacon The iBeacon you wish to configure.
	 * @param receiver The receiver that will receive updates about the state of configuration.
	 * @return An instance of ProximiioConfig that can be used to configure the iBeacon, or null if the supplied iBeacon isn't Proximiio.
	 */
	public ProximiioConfig startConfig(ProximiioBeacon beacon, Activity activity, ProximiioConfigReceiver receiver) {
//		Log.d(TAG, "ProximiioConfig startConfig");
		if (beacon.isProximiio()) {
			config = new ProximiioConfig(beacon, activity, receiver, this);
			sendMessage(beaconMessenger, ProximiioService.START_CONFIG, null);
			return config;
		}
		else {
			Log.d(tag, "iBeacon being configured is not Proximiio type!");
			receiver.error(ProximiioConfigReceiver.FailureReason.NOT_PROXIMIIO);
			return null;
		}
	}

	protected void endConfig() {
		config = null;
		sendMessage(beaconMessenger, ProximiioService.END_CONFIG, null);
	}
	
	private void notification(Intent intent, String title, String content) {
		PendingIntent contentIntent = PendingIntent.getActivity(activity, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
		NotificationManager nm = (NotificationManager)activity.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification.Builder builder = new Notification.Builder(activity);
		
		int image = activity.getResources().getIdentifier("drawable/navture512", null, activity.getPackageName());
		
		builder.setContentIntent(contentIntent)
		.setSmallIcon(image)
        .setContentTitle(title)
        .setContentText(content);
		
		Notification n = builder.build();
		n.flags |= Notification.FLAG_AUTO_CANCEL;
		
		nm.notify(1, n);
	}
}
