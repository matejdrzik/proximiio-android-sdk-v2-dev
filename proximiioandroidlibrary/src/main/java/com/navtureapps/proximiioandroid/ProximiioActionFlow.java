package com.navtureapps.proximiioandroid;

import java.util.ArrayList;

/**
 * Represents action flows designed in the web portal, contains outputs.
 */
public class ProximiioActionFlow {
    private String id;
    private String inputID;
    private ArrayList<ProximiioOutput> outputs;
    private ArrayList<String> outputStrings;

    protected ArrayList<String> getOutputStrings() {
        return outputStrings;
    }

    /**
     * Returns a list with all the outputs of this action flow.
     * @return outputs
     */
    public ArrayList<ProximiioOutput> getOutputs() {
        return outputs;
    }

    protected String getInputID() {
        return inputID;
    }

    protected String getID() {
        return id;
    }

    protected ProximiioActionFlow(String id, String inputID) {
        this.id = id;
        this.inputID = inputID;
        outputStrings = new ArrayList<>();
        outputs = new ArrayList<>();
    }

    protected void addOutput(String output) {
        outputStrings.add(output);
    }

    protected void addOutput(ProximiioOutput output) {
        outputs.add(output);
    }

    protected void clearOutputs() {
        outputs.clear();
    }
}
