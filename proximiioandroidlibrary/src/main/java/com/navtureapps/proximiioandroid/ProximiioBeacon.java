package com.navtureapps.proximiioandroid;

import android.bluetooth.BluetoothDevice;
import android.util.Log;

import com.firebase.client.utilities.Utilities;

import java.util.Arrays;

/**
 * Created by wired on 28/08/15.
 */

public class ProximiioBeacon {
    public static enum Proximity
    {
        UNKNOWN(0),  IMMEDIATE(1),  NEAR(2),  FAR(3);
        private int value;
        private Proximity(int value)
        {
            this.value = value;
        }
        public int getValue()
        {
            return this.value;
        }
    }

    public static final int BEACON_TYPE_IBEACON = 0;
    public static final int BEACON_TYPE_EDDYSTONE = 1;

    private static final char[] hexArray = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
    private static final String TAG = "ProximiioBeacon";

    protected int beaconType;
    protected String uuid;
    protected int major;
    protected int minor;
    protected Proximity proximity;
    protected Double accuracy;
    protected int rssi;
    protected int txPower;
    protected Double runningAverageRssi = null;
    protected long lastSeen;
    protected long realLastSeen;
    protected BluetoothDevice device;
    protected String namespace;
    protected String instanceId;
    protected float batteryVoltage;
    protected float temperature;
    protected float uptime;
    protected float pdu;
    protected String url;
    protected String mac;

    protected byte eddystoneFrameType;

    /**
     * Eddystone-UID frame type value.
     */
    static final byte EDDYSTONE_UID_FRAME_TYPE = 0x00;

    /**
     * Eddystone-URL frame type value.
     */
    static final byte EDDYSTONE_URL_FRAME_TYPE = 0x10;

    /**
     * Eddystone-TLM frame type value.
     */
    static final byte EDDYSTONE_TLM_FRAME_TYPE = 0x20;

    /**
     * Minimum expected Tx power (in dBm) in UID and URL frames.
     */
    static final int EDDYSTONE_MIN_EXPECTED_TX_POWER = -100;

    /**
     * Maximum expected Tx power (in dBm) in UID and URL frames.
     */
    static final int EDDYSTONE_MAX_EXPECTED_TX_POWER = 20;

    public String getMac() { return this.mac; }
    public BluetoothDevice getDevice()
    {
        return this.device;
    }

    public double getAccuracy()
    {
        if (this.accuracy == null)
        {
            double rssi2 = this.rssi;
            if (this.runningAverageRssi != null) {
                rssi2 = this.runningAverageRssi.doubleValue();
            }
            this.accuracy = Double.valueOf(calculateAccuracy(this.txPower, rssi2));
        }
        return this.accuracy.doubleValue();
    }

    public int getMajor()
    {
        return this.major;
    }

    public int getMinor()
    {
        return this.minor;
    }

    public Proximity getProximity()
    {
        if (this.proximity == null) {
            this.proximity = calculateProximity(getAccuracy());
        }
        return this.proximity;
    }

    public int getRssi()
    {
        return this.rssi;
    }

    public int getTxPower()
    {
        return this.txPower;
    }

    public String getUUID()
    {
        return this.uuid;
    }

    public String getNamespace() { return this.namespace; }
    public String getInstanceId() { return this.instanceId; }
    public String getUrl() { return this.url; }
    public float getBatteryVoltage() { return this.batteryVoltage; }
    public float getTemperature() { return this.temperature; }
    public float getUptime() { return this.uptime; }
    public float getPdu() { return this.pdu; }

    protected Boolean telemetry = false;

    public Boolean hasTelemetry() { return telemetry; }

    public boolean isProximiio()
    {
        return this.uuid.equals("2E1A0B14-BFAB-40D7-88F8-993BC447BE06");
    }

    public static ProximiioBeacon fromScanData(byte[] scanData, int rssi, BluetoothDevice device, ProximiioBeacon existingBeacon)
    {
        if (scanData.length > 0) {
            int startByte = 0;
            boolean iBeaconPatternFound = false;
            boolean eddystoneBeaconPatternFound = false;
            boolean eddystoneTLMFound = false;

            // detect ibeacon
            for (startByte = 5; startByte >= 0; startByte--) {
                if (((scanData[startByte] & 0xFF) == 76) && ((scanData[(startByte + 1)] & 0xFF) == 0) && ((scanData[(startByte + 2)] & 0xFF) == 2) && ((scanData[(startByte + 3)] & 0xFF) == 21)) {
                    iBeaconPatternFound = true;
                    break;
                }
            }

            // detect eddystone
            if (!iBeaconPatternFound) {
                for (startByte = 5; startByte >= 0; startByte--) {
                    if (((scanData[startByte] & 0xFF) == 2) && ((scanData[(startByte + 1)] & 0xFF) == 1) && ((scanData[(startByte + 2)] & 0xFF) == 6) && ((scanData[(startByte + 3)] & 0xFF) == 3)) {
                        eddystoneBeaconPatternFound = true;
                        break;
                    }
                }
            }

            // no beacon type recognized
            if (!iBeaconPatternFound && !eddystoneBeaconPatternFound && !eddystoneTLMFound) {
                return null;
            }

            ProximiioBeacon beacon = null;
            if (existingBeacon == null) {
                beacon = new ProximiioBeacon();
            } else {
                beacon = existingBeacon;
            }
            beacon.mac = device.getAddress();

            // setup ibeacon if found
            if (iBeaconPatternFound) {
                beacon.beaconType = BEACON_TYPE_IBEACON;
                beacon.major = ((scanData[(startByte + 20)] & 0xFF) * 256 + (scanData[(startByte + 21)] & 0xFF));
                beacon.minor = ((scanData[(startByte + 22)] & 0xFF) * 256 + (scanData[(startByte + 23)] & 0xFF));
                beacon.txPower = scanData[(startByte + 24)];
                beacon.rssi = rssi;
                beacon.device = device;
                beacon.lastSeen = System.nanoTime();
            }

            // setupeddystone if found
            if (eddystoneBeaconPatternFound) {
                beacon.beaconType = BEACON_TYPE_EDDYSTONE;
                beacon.txPower = (int) scanData[1];
                beacon.rssi = rssi;
                beacon.device = device;
                beacon.lastSeen = System.nanoTime();

                byte[] frameTypeBytes = new byte[1];
                System.arraycopy(scanData, 11, frameTypeBytes, 0, 1);

                if ((frameTypeBytes[0] & 0xFF) == EDDYSTONE_UID_FRAME_TYPE) {
                    // frame is eddystone uid

                    // namespace
                    byte[] namespaceBytes = new byte[10];
                    System.arraycopy(scanData, 13, namespaceBytes, 0, 10);
                    beacon.namespace = toHexString(namespaceBytes);

                    // instanceid
                    byte[] instanceIdBytes = new byte[6];
                    System.arraycopy(scanData, 23, instanceIdBytes, 0, 6);
                    beacon.instanceId = toHexString(instanceIdBytes);
                }

                if ((frameTypeBytes[0] & 0xFF) == EDDYSTONE_TLM_FRAME_TYPE) {
                    // frame is eddystone tlm

                    byte[] uid = new byte[12];
                    System.arraycopy(scanData, 0, uid, 0, 12);
                    byte[] txPower = new byte[1];
                    System.arraycopy(scanData, 12, txPower, 0, 1);

                    byte[] voltage = new byte[2];
                    System.arraycopy(scanData, 13, voltage, 0, 2);

                    byte[] temperature = new byte[2];
                    System.arraycopy(scanData, 15, temperature, 0, 2);

                    byte[] pdu = new byte[4];
                    System.arraycopy(scanData, 17, pdu, 0, 4);

                    byte[] timeSince = new byte[4];
                    System.arraycopy(scanData, 21, timeSince, 0, 4);

                    beacon.telemetry = true;
                    beacon.txPower = hex2decimal(toHexString(txPower));
                    beacon.batteryVoltage = hex2decimal(toHexString(voltage)) / 1000.0f;
                    beacon.temperature = hex2decimal(toHexString(temperature)) / 256.0f;
                    beacon.pdu = hex2decimal(toHexString(pdu));
                    beacon.uptime = hex2decimal(toHexString(timeSince)) / 10;

                }
            }

            beacon.realLastSeen = System.nanoTime();

            byte[] proximityUuidBytes = new byte[16];
            System.arraycopy(scanData, startByte + 4, proximityUuidBytes, 0, 16);
            String hexString = bytesToHex(proximityUuidBytes);
            StringBuilder sb = new StringBuilder();
            sb.append(hexString.substring(0, 8));
            sb.append("-");
            sb.append(hexString.substring(8, 12));
            sb.append("-");
            sb.append(hexString.substring(12, 16));
            sb.append("-");
            sb.append(hexString.substring(16, 20));
            sb.append("-");
            sb.append(hexString.substring(20, 32));

            beacon.uuid = sb.toString();

            return beacon;
        } else {
            return null;
        }
    }

    protected ProximiioBeacon(ProximiioBeacon otherBeacon)
    {
        if (this.beaconType == BEACON_TYPE_IBEACON) {
            this.major = otherBeacon.major;
            this.minor = otherBeacon.minor;
            this.uuid = otherBeacon.uuid;
        } else if (this.beaconType == BEACON_TYPE_EDDYSTONE) {
            this.uuid = otherBeacon.uuid;
            this.namespace = otherBeacon.namespace;
            this.instanceId = otherBeacon.instanceId;
        }
        update(otherBeacon);
    }

    // init ibeacon
    protected ProximiioBeacon(String proximityUuid, int major, int minor, int txPower, int rssi)
    {
        this.beaconType = BEACON_TYPE_IBEACON;
        this.uuid = proximityUuid;
        this.rssi = rssi;
        this.txPower = txPower;
        this.major = major;
        this.minor = minor;
    }

    // init eddystone
    protected ProximiioBeacon(String namespace, String instanceId, int txPower, int rssi) {
        this.beaconType = BEACON_TYPE_EDDYSTONE;
        this.namespace = namespace;
        this.instanceId = instanceId;
        this.txPower = txPower;
        this.rssi = rssi;
    }

    public ProximiioBeacon(String proximityUuid, int major, int minor) {
        this(proximityUuid, major, minor, 0, 0);
    }

    public ProximiioBeacon(String namespace, String instanceId) {
        this(namespace, instanceId, 0, 0);
    }

    private ProximiioBeacon() {}

    protected void update(ProximiioBeacon copy)
    {
        this.accuracy = copy.accuracy;
        this.proximity = copy.proximity;
        this.rssi = copy.rssi;
        this.runningAverageRssi = copy.runningAverageRssi;
        this.txPower = copy.txPower;
        this.realLastSeen = System.nanoTime();
        if (copy.hasTelemetry()) {
            this.telemetry = true;
            this.batteryVoltage = copy.batteryVoltage;
            this.temperature = copy.temperature;
            this.pdu = copy.pdu;
            this.uptime = copy.uptime;
        }
        this.batteryVoltage = copy.batteryVoltage;
    }

    protected void updateTime()
    {
        this.lastSeen = System.nanoTime();
    }

    public long millisecondsAgo()
    {
        return (System.nanoTime() - this.lastSeen) / 1000000L;
    }

    public long realMillisecondsAgo()
    {
        return (System.nanoTime() - this.realLastSeen) / 1000000L;
    }

    public boolean equals(ProximiioBeacon beacon)
    {
        if (beaconType == beacon.beaconType) {
            if (beaconType == ProximiioBeacon.BEACON_TYPE_IBEACON) {
                return (getUUID().equals(beacon.getUUID())) && (getMajor() == beacon.getMajor()) && (getMinor() == beacon.getMinor());
            } else if (beaconType == ProximiioBeacon.BEACON_TYPE_EDDYSTONE) {
                return (getNamespace().equals(beacon.getNamespace())) && (getInstanceId().equals(beacon.getInstanceId()));
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    protected static double calculateAccuracy(int txPower, double rssi)
    {
        if (rssi == 0.0D) {
            return -1.0D;
        }
        double ratio = rssi * 1.0D / txPower;
        if (ratio < 1.0D) {
            return Math.pow(ratio, 10.0D);
        }
        double accuracy = 0.89976D * Math.pow(ratio, 7.7095D) + 0.111D;
        return accuracy;
    }

    protected static Proximity calculateProximity(double accuracy)
    {
        if (accuracy < 0.0D) {
            return Proximity.UNKNOWN;
        }
        if (accuracy < 0.5D) {
            return Proximity.IMMEDIATE;
        }
        if (accuracy <= 4.0D) {
            return Proximity.NEAR;
        }
        return Proximity.FAR;
    }

    private static String bytesToHex(byte[] bytes)
    {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++)
        {
            int v = bytes[j] & 0xFF;
            hexChars[(j * 2)] = hexArray[(v >>> 4)];
            hexChars[(j * 2 + 1)] = hexArray[(v & 0xF)];
        }
        return new String(hexChars);
    }

    private static final char[] HEX = "0123456789ABCDEF".toCharArray();

    static String toHexString(byte[] bytes) {
        char[] chars = new char[bytes.length * 2];
        for (int i = 0; i < bytes.length; i++) {
            int c = bytes[i] & 0xFF;
            chars[i * 2] = HEX[c >>> 4];
            chars[i * 2 + 1] = HEX[c & 0x0F];
        }
        return new String(chars).toLowerCase();
    }

    public boolean onRange(Proximity proximity)
    {
        return proximity.getValue() >= getProximity().getValue();
    }

    public static int hex2decimal(String s) {
        String digits = "0123456789ABCDEF";
        s = s.toUpperCase();
        int val = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int d = digits.indexOf(c);
            val = 16*val + d;
        }
        return val;
    }

}
