package com.navtureapps.proximiioandroid;

/**
 * Created by wired on 29/08/15.
 */

import android.util.Log;

import java.util.ArrayList;

public class ProximiioBeaconHelper {
    private static final String TAG = "ProximiioBeaconHelper";
    private ProximiioBeaconReceiver beaconReceiver;
    private Proximiio proximiio;

    protected void setProximiio(Proximiio proximiio) {
        this.proximiio = proximiio;
    }

    protected ProximiioBeaconReceiver getReceiver() {
        return beaconReceiver;
    }

    protected ProximiioBeaconHelper(Proximiio newProximiio) {
        this.proximiio = newProximiio;
        beaconReceiver = new ProximiioBeaconReceiver() {
            @Override
            public void foundBeacon(final ProximiioBeacon beacon) {
                ArrayList<ProximiioInput> input = proximiio.generateBeaconEvent(beacon, true);
                proximiio.updateBeaconPositions(input);
            }

            @Override
            public void lostBeacon(final ProximiioBeacon beacon, final LostReason lostReason) {
                proximiio.generateBeaconEvent(beacon, false);
            }

            @Override
            public void updatedBeacon(final ProximiioBeacon beacon) {
                if (beacon.beaconType == ProximiioBeacon.BEACON_TYPE_EDDYSTONE) {
                    proximiio.updateEddystoneTelemetry(beacon);
                }
                proximiio.updateBeaconPositions(proximiio.getInput(beacon));
            }

            @Override
            public String getReceiverID() {
                return proximiio.appID;
            }
        };
    }
}