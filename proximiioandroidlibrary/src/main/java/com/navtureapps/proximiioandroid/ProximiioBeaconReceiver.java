package com.navtureapps.proximiioandroid;

//Copyright � 2015 NavtureApps (http://navtureapps.com/)

public abstract class ProximiioBeaconReceiver extends ProximiioFiltered {

    private final String tag = "ProximiioBeaconReceiver";

    /**
     * Used to represent the reason why an iBeacon was lost.
     */
    public enum LostReason {

        /**
         * Occurs when an iBeacon is lost due to it being too far away, and not
         * being picked up by Bluetooth scan anymore.
         */
        TIMED_OUT,

        /**
         * When a ProximiioBeaconReceiver is removed from the list of active
         * listeners at ProximiioService. This happens when destroy is called on
         * ProximiioAPI, either manually or when the Activity calls onDestroy.
         */
        RECEIVER_UNREGISTERED,

        /**
         * Used when scanning is forcefully stopped.
         */
        SCAN_STOP,

        /**
         * This happens when an iBeacon is being configured; all other Bluetooth
         * connections are dropped and no Beacons can be scanned.
         */
        CONFIGURING
    }

    /**
     * Called once an iBeacon is found. Following "founds" are indicated with
     * the updatedBeacon function if the iBeacon is not lost.
     *
     * @param beacon
     *            The iBeacon that was found.
     */
    public abstract void foundBeacon(final ProximiioBeacon beacon);

    /**
     * Called once an iBeacon has been unavailable for a few scans. If found
     * again, foundBeacon will be called.
     *
     * @param beacon
     *            The iBeacon that was lost.
     * @param reason
     *            Why this iBeacon was lost. See LostReason.
     */
    public abstract void lostBeacon(final ProximiioBeacon beacon,
                                     LostReason reason);

    /**
     * Called at each update about a known iBeacon. At the first discovery,
     * foundBeacon will be called instead. This will be called only once per
     * scan.
     *
     * @param beacon
     *            The iBeacon that was updated.
     */
    public abstract void updatedBeacon(final ProximiioBeacon beacon);

    /**
     * This should return a unique yet consistent ID for an implementation (for
     * example, the application package identifier). Every time an interface is
     * hooked up, the service checks if that interface is already being served.
     * This implementation doesn't require the API user to check if the service
     * is already hooked up, and makes it safe to create an instance of the API
     * class on every Actrivity's onCreate. If you want to run multiple
     * listeners, use different IDs for them or they will be considered the
     * same, and all but one will be dropped.
     *
     * @return This implementation's ID.
     */
    public abstract String getReceiverID();

    /**
     * Should this receiver receive an updatedBeacon event every time it's
     * found? The default is false.
     *
     * @return true for yes, false for once per BeaconValid (a scan cycle by
     *         default).
     */
    public boolean fastUpdate() {
        return false;
    }

    @Override
    protected String getTag() {
        return tag;
    }
}