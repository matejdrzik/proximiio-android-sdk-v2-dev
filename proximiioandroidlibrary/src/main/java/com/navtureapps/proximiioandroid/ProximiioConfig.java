//Copyright � 2014 NavtureApps (http://navtureapps.com/)

package com.navtureapps.proximiioandroid;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.os.Handler;
import android.util.Log;

public class ProximiioConfig {
	private boolean valid;
	private BluetoothGatt gatt;
	private ProximiioBeacon beacon;
	private ProximiioConfigReceiver receiver;
	private BluetoothDevice device;
	private String tag;
	private BluetoothGattService service;
	private LinkedList<BluetoothGattCharacteristic> writeQueue;
	private LinkedList<BluetoothGattCharacteristic> readQueue;
	private boolean writing;
	private boolean reading;
	private boolean initRead;
	private boolean closed;
	private ProximiioAPI api;
	private BluetoothGattCallback callback;

	private final static UUID SERVICE_UUID = UUID
			.fromString("0000fff0-0000-1000-8000-00805f9b34fb");
	private final static HashMap<Characteristics, UUID> uuidMap = new HashMap<Characteristics, UUID>();

	private static enum Characteristics {
		UUID, MAJOR, MINOR, MEASURED_POWER, TRANSMISSION_POWER, PASSWORD, BROADCAST_INTERVAL, SERIAL_ID, CONNECTION_MODE, REBOOT
	}

	static {
		uuidMap.put(Characteristics.UUID,
				UUID.fromString("0000fff1-0000-1000-8000-00805f9b34fb"));
		uuidMap.put(Characteristics.MAJOR,
				UUID.fromString("0000fff2-0000-1000-8000-00805f9b34fb"));
		uuidMap.put(Characteristics.MINOR,
				UUID.fromString("0000fff3-0000-1000-8000-00805f9b34fb"));
		uuidMap.put(Characteristics.MEASURED_POWER,
				UUID.fromString("0000fff4-0000-1000-8000-00805f9b34fb"));
		uuidMap.put(Characteristics.TRANSMISSION_POWER,
				UUID.fromString("0000fff5-0000-1000-8000-00805f9b34fb"));
		uuidMap.put(Characteristics.PASSWORD,
				UUID.fromString("0000fff6-0000-1000-8000-00805f9b34fb"));
		uuidMap.put(Characteristics.BROADCAST_INTERVAL,
				UUID.fromString("0000fff7-0000-1000-8000-00805f9b34fb"));
		uuidMap.put(Characteristics.SERIAL_ID,
				UUID.fromString("0000fff8-0000-1000-8000-00805f9b34fb"));
		uuidMap.put(Characteristics.CONNECTION_MODE,
				UUID.fromString("0000fffe-0000-1000-8000-00805f9b34fb"));
		uuidMap.put(Characteristics.REBOOT,
				UUID.fromString("0000ffff-0000-1000-8000-00805f9b34fb"));
	}

	protected ProximiioConfig(ProximiioBeacon iBeacon,
			final Activity activity, final ProximiioConfigReceiver receiver, ProximiioAPI api) {
		this.api = api;
		this.beacon = iBeacon;
		device = iBeacon.getDevice();
		valid = true;
		writing = false;
		reading = false;
		initRead = false;
		closed = false;
		tag = "ProximiioConfig";
		this.receiver = receiver;
		writeQueue = new LinkedList<BluetoothGattCharacteristic>();
		readQueue = new LinkedList<BluetoothGattCharacteristic>();

		callback = new BluetoothGattCallback() {
			@Override
			public void onConnectionStateChange(final BluetoothGatt gatt,
					int status, int newState) {
				if (status == BluetoothGatt.GATT_SUCCESS) {
					if (newState == BluetoothGatt.STATE_CONNECTED) {
						Log.d(tag, "Authenticating...");

						final BluetoothAdapter adapter = BluetoothAdapter
								.getDefaultAdapter();

						final Handler handler = new Handler(
								activity.getMainLooper());
						handler.post(new Runnable() {
							@Override
							public void run() {
								if (valid) {
									Set<BluetoothDevice> pairedDevices = adapter
											.getBondedDevices();
									if (pairedDevices.contains(device)) {
										Log.d(tag,
												"Connection success, discovering services...");
										gatt.discoverServices();
									} else {
										handler.postDelayed(this, 1000);
									}
								}
							}
						});
					} else if (newState == BluetoothGatt.STATE_DISCONNECTED
							&& !closed) {
						Log.d(tag, "Connection failure, retrying...");
						connect(activity);
					}
				} else if (status == BluetoothGatt.GATT_INSUFFICIENT_ENCRYPTION
						|| status == BluetoothGatt.GATT_INSUFFICIENT_AUTHENTICATION) {
					valid = false;
					Log.e(tag, "Connection failure: wrong passkey");
					receiver.error(ProximiioConfigReceiver.FailureReason.WRONG_PAIRING_PASSKEY);
					connect(activity);
				}
			}

			@Override
			public void onServicesDiscovered(BluetoothGatt gatt, int status) {
				service = gatt.getService(SERVICE_UUID);
				Log.d(tag, "Services discovered, reading values...");

				List<BluetoothGattCharacteristic> chars = service
						.getCharacteristics();
				for (int i = 0; i < chars.size(); i++) {
					BluetoothGattCharacteristic c = chars.get(i);
					if ((BluetoothGattCharacteristic.PROPERTY_READ & c
							.getProperties()) == BluetoothGattCharacteristic.PROPERTY_READ) {
						readCharacteristic(chars.get(i));
					}
				}

				if (readQueue.isEmpty()) {
					confirmInitReads();
				} else {
					nextRead();
				}
			}

			@Override
			public void onCharacteristicWrite(BluetoothGatt gatt,
					BluetoothGattCharacteristic characteristic, int status) {
				if (status != BluetoothGatt.GATT_SUCCESS) {
					Log.e(tag, "Characteristic write failed!");
				}
				nextWrite();
			};

			@Override
			public void onCharacteristicRead(BluetoothGatt gatt,
					BluetoothGattCharacteristic characteristic, int status) {
				if (status == BluetoothGatt.GATT_SUCCESS) {
					switch (characteristic.getValue().length) {
					case 2:
						Log.d(tag,
								String.valueOf(getShortValue(characteristic)));
						break;
					case 1:
						Log.d(tag, String.valueOf((int) characteristic
								.getValue()[0]));
						break;
					default:
						Log.d(tag, bytesToHex(characteristic.getValue()));
						break;
					}
				} else {
					Log.e(tag, "Characteristic read failed!");
				}

				nextRead();
			}
		};

		Log.d(tag, "Connecting to beacon...");
		connect(activity);
	}

	private void confirmInitReads() {
		if (!initRead) {
			Log.d(tag, "Values read, calling success on receiver.");
			initRead = true;
			receiver.success();
		}
	}

	private void connect(final Activity activity) {
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				readQueue.clear();
				writeQueue.clear();
				valid = true;
				gatt = device.connectGatt(activity.getApplicationContext(),
						false, callback);
			}
		});
	}

	/**
	 * Make sure you call this when you're done with configuring the beacon.
	 * Makes iBeacons available to scan again.
	 */
	public void destroy() {
		if (gatt != null) {
			closed = true;
			gatt.close();
			gatt = null;
			api.endConfig();
			Log.d(tag, "Connection closed.");
		} else {
			Log.d(tag, "Connection already closed.");
		}
	}

	private String bytesToHex(byte[] bytes) {
		char[] hexArray = "0123456789ABCDEF".toCharArray();
		char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}

	private short getShortValue(BluetoothGattCharacteristic characteristic) {
		ByteBuffer bb = ByteBuffer.wrap(characteristic.getValue());
		return bb.getShort();
	}

	private void writeCharacteristic(BluetoothGattCharacteristic characteristic) {
		if (gatt != null) {
			writeQueue.add(characteristic);
			if (!writing) {
				writing = true;
				nextWrite();
			}
		} else {
			Log.e(tag, "Trying to write with a closed connection!");
		}
	}

	private void readCharacteristic(BluetoothGattCharacteristic characteristic) {
		if (gatt != null) {
			readQueue.add(characteristic);
			if (!writing && !reading) {
				reading = true;
				nextRead();
			}
		} else {
			Log.e(tag, "Trying to read with a closed connection!");
		}
	}

	private void nextWrite() {
		if (!writeQueue.isEmpty()) {
			gatt.writeCharacteristic(writeQueue.remove());
		} else {
			writing = false;
			Log.d(tag,
					"All queued writes complete, calling writesComplete on receiver.");
			receiver.writesComplete();
			nextRead();
		}
	}

	private void nextRead() {
		if (writeQueue.isEmpty()) {
			if (!readQueue.isEmpty()) {
				gatt.readCharacteristic(readQueue.remove());
			} else {
				reading = false;
				confirmInitReads();
			}
		}
	}

	private void writeNewValue(Characteristics key, byte[] value) {
		BluetoothGattCharacteristic c = service.getCharacteristic(uuidMap
				.get(key));
		c.setValue(value);
		writeCharacteristic(c);
	}

	private byte[] threeOctet(int value) {
		return new byte[] { (byte) (value >>> 16), (byte) (value >>> 8),
				(byte) value };
	}

	private void writeNewShort(Characteristics key, short value) {
		ByteBuffer buffer = ByteBuffer.allocate(2);
		buffer.putShort(value);
		buffer.flip();
		writeNewValue(key, buffer.array());
	}

	/**
	 * Get the beacon associated with the current configuration process.
	 * 
	 * @return beacon if found, otherwise null.
	 */
	public ProximiioBeacon getBeacon() {
		return beacon;
	}

	/**
	 * REBOOT the beacon being configured. This is required for any changes to
	 * take effect.
	 * 
	 * @param password
	 *            The current pairing password for confirmation. Note that if
	 *            you're changing the pairing password, the old password is
	 *            expected for this.
	 */
	public void rebootIBeacon(int password) { // 362358 for default
		writeNewValue(Characteristics.REBOOT, threeOctet(password));
	}

	/**
	 * Set a new major value.
	 * 
	 * @param major
	 *            The new major.
	 */
	public void setMajor(short major) {
		writeNewShort(Characteristics.MAJOR, major);
	}

	/**
	 * Set a new minor value.
	 * 
	 * @param minor
	 *            The new minor.
	 */
	public void setMinor(short minor) {
		writeNewShort(Characteristics.MINOR, minor);
	}

	/**
	 * Set the value for measured power.
	 * 
	 * @param power
	 *            The new value.
	 */
	public void setMeasuredPower(byte power) {
		writeNewValue(Characteristics.MEASURED_POWER, new byte[] { power });
	}

	/**
	 * Set the value for transmission power.
	 * 
	 * @param power
	 *            The new value. 0-3.
	 */
	public void setTransmissionPower(byte power) {
		writeNewValue(Characteristics.TRANSMISSION_POWER, new byte[] { power });
	}

	/**
	 * Set a new pairing password.
	 * 
	 * @param password
	 *            A new password. 0-16777215
	 */
	public void setPairingPassword(int password) {
		writeNewValue(Characteristics.PASSWORD, threeOctet(password));
	}

	/**
	 * Set the broadcasting interval.
	 * 
	 * @param interval
	 *            Interval in hundreds of milliseconds (100ms - 25500ms).
	 */
	public void setBroadcastingInterval(byte interval) {
		writeNewValue(Characteristics.BROADCAST_INTERVAL,
				new byte[] { interval });
	}

	/**
	 * Set a new serial ID.
	 * 
	 * @param id
	 *            A new serial ID.
	 */
	public void setSerialID(short id) {
		writeNewShort(Characteristics.SERIAL_ID, id);
	}

	/**
	 * Set the connection mode for this beacon. The beacon will be reachable
	 * for 5 minutes after a reset, after that it'll follow this setting.
	 * 
	 * @param mode
	 *            0 for connectable mode, anything else for non-connectable.
	 */
	public void setConnectionMode(byte mode) {
		writeNewValue(Characteristics.CONNECTION_MODE, new byte[] { mode });
	}

	/**
	 * MAJOR of the beacon.
	 * 
	 * @return MAJOR value.
	 */
	public short getMajor() {
		return getShortValue(service.getCharacteristic(uuidMap
				.get(Characteristics.MAJOR)));
	}

	/**
	 * Minor of the beacon.
	 * 
	 * @return Minor value.
	 */
	public short getMinor() {
		return getShortValue(service.getCharacteristic(uuidMap
				.get(Characteristics.MINOR)));
	}

	/**
	 * The measured power of the beacon.
	 * 
	 * @return Measured power.
	 */
	public byte getMeasuredPower() {
		return service.getCharacteristic(
				uuidMap.get(Characteristics.MEASURED_POWER)).getValue()[0];
	}

	/**
	 * The transmission power of the beacon. 0-3. The mode 1 only works on
	 * certain versions.
	 * 
	 * @return Transmission power.
	 */
	public byte getTransmissionPower() {
		return service.getCharacteristic(
				uuidMap.get(Characteristics.TRANSMISSION_POWER)).getValue()[0];
	}

	/**
	 * The broadcasting interval in hundreds of milliseconds (100ms - 25500ms).
	 * 
	 * @return Broadcasting interval.
	 */
	public byte getBroadcastingInterval() {
		return service.getCharacteristic(
				uuidMap.get(Characteristics.BROADCAST_INTERVAL)).getValue()[0];
	}

	/**
	 * The serial ID of the beacon being configured.
	 * 
	 * @return A short representing the serial ID.
	 */
	public short getSerialID() {
		return getShortValue(service.getCharacteristic(uuidMap
				.get(Characteristics.SERIAL_ID)));
	}

	/**
	 * The connection mode for this beacon.
	 * 
	 * @return 0 for connectable, anything else for non-connectable.
	 */
	public byte getConnectionMode() {
		return service.getCharacteristic(
				uuidMap.get(Characteristics.CONNECTION_MODE)).getValue()[0];
	}
}
