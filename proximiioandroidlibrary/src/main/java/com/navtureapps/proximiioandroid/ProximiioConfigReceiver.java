//Copyright � 2014 NavtureApps (http://navtureapps.com/)

package com.navtureapps.proximiioandroid;

/**
 * Used to signal the state of an iBeacon being configured.
 */
public interface ProximiioConfigReceiver {

	/**
	 * Represents the reason of failure in case of an error.
	 */
	public enum FailureReason {
		/**
		 * The iBeacon wasn't Proximiio-supplied.
		 */
		NOT_PROXIMIIO,

		/**
		 * The pairing passkey entered by the user was incorrect.
		 */
		WRONG_PAIRING_PASSKEY
	}

	/**
	 * An error occured.
	 * 
	 * @param reason
	 *            The reason why something went wrong.
	 */
	public void error(FailureReason reason);

	/**
	 * Bluetooth pairing was succesful and all the values of the iBeacon are
	 * available. Now you can start configuring the iBeacon.
	 */
	public void success();

	/**
	 * Called when all queued writes to the BLE device are completed. For
	 * example: <br/>
	 * <br/>
	 * <code>
	 * proximiioConfig.setMajor(100); <br/>
	 * proximiioConfig.setMinor(500); <br/>
	 * proximiioConfig.rebootIBeacon(1234); //Current pairing password is needed for confirmation. <br/>
	 * </code>
	 * <br/>
	 * These calls will queue writes to the Bluetooth Low Energy device being
	 * configured. When all of them have finished, this function will be called.
	 */
	public void writesComplete();
}
