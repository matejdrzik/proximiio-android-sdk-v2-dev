package com.navtureapps.proximiioandroid;

import android.location.Location;

public class ProximiioCoordinates {
    private double latitude;
    private double longitude;
    private double radius;

    protected double getLat() {
        return latitude;
    }

    protected double getLon() {
        return longitude;
    }

    protected double getRadius() {
        return radius;
    }

    protected ProximiioCoordinates(double latitude, double longitude, double radius) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.radius = radius;
    }

    protected boolean contains(double latitude, double longitude) {
        float[] results = new float[1];
        Location.distanceBetween(this.latitude, this.longitude, latitude, longitude, results);
        return results[0] < radius;
    }
}
