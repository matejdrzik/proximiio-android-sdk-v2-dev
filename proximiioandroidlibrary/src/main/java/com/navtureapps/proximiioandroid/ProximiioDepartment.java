package com.navtureapps.proximiioandroid;

import android.os.Handler;
import android.util.Log;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;

import java.util.ArrayList;
import java.util.Map;

public class ProximiioDepartment {

    private static final String TAG = "ProximiioDepartment";
    private String id;
    private ValueEventListener polygonListener;
    private ValueEventListener tagListener;
    private boolean found;
    private Firebase firebase;
    private String tag;
    private Polygon geometry;
    private GeometryFactory factory;
    private ArrayList<String> tags;
    private boolean entered;
    private String placeID;
    private boolean destroyed;
    private String name;
    private ProximiioPlace place;
    private Runnable runnable;
    private boolean postedRunnable;
    private static Handler handler;

    protected String getID() {
        return id;
    }

    protected boolean getFound() {
        return found;
    }

    protected void setFound(boolean found) {
        this.found = found;
    }

    protected ArrayList<String> getTags() {
        return tags;
    }

    protected String getName() {
        return name;
    }

    protected void setName(String name) {
        this.name = name;
    }

    protected String getPlaceID() {
        return placeID;
    }

    protected void setPlaceID(String placeID) {
        this.placeID = placeID;
    }

    protected boolean getDestroyed() {
        return destroyed;
    }

    protected ProximiioPlace getPlace() {
        return place;
    }

    protected void setPlace(ProximiioPlace place) {
        this.place = place;
    }

    protected ProximiioDepartment(Firebase firebase, String placeID, String name) {
        this.id = firebase.getKey();
        this.firebase = firebase;
        entered = false;
        factory = new GeometryFactory();
        tag = "ProximiioDepartment";
        tags = new ArrayList<>();
        this.placeID = placeID;
        this.name = name;
        destroyed = false;
        postedRunnable = false;

        if (handler == null) {
            handler = new Handler();
        }

        runnable = new Runnable() {
            @Override
            public void run() {
                entered = false;
                postedRunnable = false;
            }
        };

        polygonListener = new ValueEventListener() {
            @SuppressWarnings("unchecked")
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<Map<String, Double>> data = (ArrayList<Map<String, Double>>) dataSnapshot.getValue();
                if (data != null) {
                    Coordinate[] coordinates = new Coordinate[data.size() + 1];
                    for (int i = 0; i < data.size(); i++) {
                        Map<String, Double> coords = data.get(i);
                        coordinates[i] = new Coordinate(coords.get("lat"), coords.get("lng"));
                    }

                    coordinates[coordinates.length - 1] = coordinates[0];

                    geometry = factory.createPolygon(factory.createLinearRing(coordinates), null);
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Log.e(tag, firebaseError.getMessage());
            }
        };

        tagListener = new ValueEventListener() {
            @SuppressWarnings("unchecked")
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                tags = (ArrayList<String>) dataSnapshot.getValue();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Log.e(tag, firebaseError.getMessage());
            }
        };

        firebase.child("latLngs").addValueEventListener(polygonListener);
        firebase.child("tags").addValueEventListener(tagListener);
    }

    protected boolean contains(double lat, double lon, double accuracy) {
        Point point = factory.createPoint(new Coordinate(lat, lon));
        boolean value = geometry != null ? geometry.contains(point) || point.isWithinDistance(geometry, accuracy / 111111) : entered;

        if (value) {
            if (!entered) {
                entered = true;
                return true;
            }
            else if (postedRunnable) {
                entered = true;
                handler.removeCallbacks(runnable);
                postedRunnable = false;
            }
        }
        else {
            if (entered && !postedRunnable) {
                //5 minute delay for leaving to reduce false positives in entering
                handler.postDelayed(runnable, 300000);
                postedRunnable = true;
            }
        }
        return false;
    }

    protected void destroy(boolean keepValid) {
        if (firebase != null) {
            firebase.child("latLngs").removeEventListener(polygonListener);
            firebase.child("tags").removeEventListener(tagListener);
            firebase = null;
        }
        destroyed = !keepValid;
    }
}
