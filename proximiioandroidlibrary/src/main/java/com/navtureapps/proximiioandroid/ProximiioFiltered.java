package com.navtureapps.proximiioandroid;

import java.util.ArrayList;

import android.util.Log;

public abstract class ProximiioFiltered {

	private static final String TAG = "ProximiioFiltered";
	protected ArrayList<ProximiioRegion> regions = new ArrayList<ProximiioRegion>();
	private final String tag = "ProximiioFiltered";

	protected String getTag() {
		return tag;
	}

	/**
	 * Add a region filter. By default, information will be received about all
	 * iBeacons. If you add regions, only information about iBeacons that fit
	 * regions' criterias are reported. You can also set the region to exclude
	 * mode (region.setExclude(true)), which changes its' behaviour so that only
	 * information about iBeacons that don't match that region is received.
	 * 
	 * @param region
	 *            The region instance to add.
	 */

	public void addRegion(ProximiioRegion region) {
		boolean valid = true;
		for (int i = 0; i < regions.size(); i++) {
			if (regions.get(i).equals(region)) {
				valid = false;
				break;
			}
		}
		if (valid) {
			regions.add(region);
			Log.d(getTag(), "Region filter added.");
		} else {
			Log.w(getTag(), "An identical region filter is already in use!");
		}
	}

	/**
	 * Remove a region filter. Tries to remove a region that {@code equals()} if
	 * no region with the supplied reference is found.
	 * 
	 * @param region
	 *            The region to remove.
	 */
	public void removeRegion(ProximiioRegion region) {
		if (regions.remove(region)) {
			Log.d(getTag(), "Region filter removed.");
		} else {
			for (int i = 0; i < regions.size(); i++) {
				ProximiioRegion region2 = regions.get(i);
				if (region2.equals(region)) {
					regions.remove(i);
					Log.d(getTag(), "Equal region filter removed.");
					return;
				}
			}
			Log.w(getTag(), "Region filter was not registered!");
		}
	}

	/**
	 * Removes all regions.
	 */
	public void removeAllRegions() {
		regions.clear();
	}

	protected boolean validBeacon(ProximiioBeacon beacon) {
		if (regions.isEmpty()) {
			return true;
		}

		boolean value = false;
		for (int i = 0; i < regions.size(); i++) {
			ProximiioRegion region = regions.get(i);
			if (region.isInRange(beacon)) {
				if (region.isExclude()) {
					return false;
				} else {
					value = true;
				}
			}
		}
		return value;
	}
}