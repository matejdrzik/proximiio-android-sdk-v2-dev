package com.navtureapps.proximiioandroid;

import android.util.Log;

import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.firebase.geofire.GeoFire;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@SuppressWarnings("ThrowableResultOfMethodCallIgnored")
public class ProximiioFirebase {
    private static final String TAG = "ProximiioFirebase";
    private Proximiio proximiio;

    protected ValueEventListener organizationListener;
    protected ValueEventListener settingsListener;
    protected ValueEventListener actionFlowListener;
    protected ValueEventListener outputListener;
    protected ValueEventListener departmentListener;
    protected ValueEventListener floorListener;
    protected ValueEventListener placeListener;
    protected Firebase firebase;
    protected GeoFire geoFire;
    protected DataSnapshot organizationData;
    protected HashMap<String, ProximiioFloor> floors;
    protected boolean indoorAtlasReady;
    protected String defaultFloor;

    private boolean error;
    private ArrayList<ProximiioPlace> places;

    protected void setProximiio(Proximiio proximiio) {
        this.proximiio = proximiio;
    }

    public ProximiioFirebase(Proximiio newProximiio, String authToken) {
        proximiio = newProximiio;
        floors = new HashMap<>();
        places = new ArrayList<>();
        indoorAtlasReady = false;

        firebase = new Firebase("https://proximiio.firebaseio.com/");
        geoFire = new GeoFire(firebase.child("geofire"));

        error = true;
        init(authToken);

        placeListener = new ValueEventListener() {
            @SuppressWarnings("unchecked")
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();
                if (value != null) {
                    places.clear();
                    for (Map.Entry<String, Object> entry : value.entrySet()) {
                        Map<String, Object> next = (Map<String, Object>) entry.getValue();
                        places.add(new ProximiioPlace(entry.getKey(), (String)next.get("name"), (String)next.get("indooratlasvenueid")));
                    }

                    resolvePlaces();
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                firebaseError.toException().printStackTrace();
            }
        };

        floorListener = new ValueEventListener() {
            @SuppressWarnings("unchecked")
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d(TAG, "floorListener:" + dataSnapshot.toString());
                Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();
                if (value != null) {
                    floors.clear();
                    defaultFloor = null;
                    for (Map.Entry<String, Object> entry : value.entrySet()) {
                        Map<String, Object> next = (Map<String, Object>) entry.getValue();
                        if (proximiio.indoorAtlasEnabled) {
                            Log.d(TAG, "indoorAtlasEnabled, looking for floor with ida floorid");
                            if (defaultFloor == null && next.get("indoorAtlasFloorID") != null && next.get("floorplanID") != null) {
                                defaultFloor = entry.getKey();
                            }
                        } else {
                            Log.d(TAG, "indoorAtlasEnabled false");
                            if (defaultFloor == null) {
                                defaultFloor = entry.getKey();
                            }
                        }
                        floors.put(entry.getKey(), new ProximiioFloor((String) next.get("indoorAtlasFloorID"), (String) next.get("floorplanID")));
                    }
                    if (!indoorAtlasReady) {
                        indoorAtlasReady = true;
                    }
                    else if (proximiio.indoorAtlas == null) {
                        proximiio.initIndoorAtlas();
                    }
                    else {
                        proximiio.cleanConnections(false, false, true, false);
                        proximiio.initIndoorAtlas();
                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                firebaseError.toException().printStackTrace();
            }
        };

        organizationListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                organizationData = dataSnapshot;
                proximiio.setUpOrganization(true, true, true, true, true, false);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                firebaseError.toException().printStackTrace();
            }
        };

        actionFlowListener = new ValueEventListener() {
            @SuppressWarnings("unchecked")
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                proximiio.actionFlows.clear();

                Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();
                if (value != null) {
                    for (Map.Entry<String, Object> entry : value.entrySet()) {
                        Map<String, Object> next = (Map<String, Object>) entry.getValue();

                        ProximiioActionFlow actionFlow = new ProximiioActionFlow(entry.getKey(), (String) next.get("input"));
                        ArrayList<Object> output = (ArrayList<Object>) next.get("outputs");
                        for (int i = 0; i < output.size(); i++) {
                            actionFlow.addOutput(((Map<String, String>) output.get(i)).get("id"));
                        }

                        proximiio.actionFlows.add(actionFlow);
                    }

                    proximiio.resolveInputs();
                    proximiio.resolveOutputs();
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                firebaseError.toException().printStackTrace();
            }
        };

        outputListener = new ValueEventListener() {
            @SuppressWarnings("unchecked")
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                proximiio.outputs.clear();

                Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();
                if (value != null) {
                    for (Map.Entry<String, Object> entry : value.entrySet()) {
                        Map<String, String> next = (Map<String, String>) entry.getValue();
                        String type = next.get("type");
                        if (type.equals(ProximiioOutput.getOutputTypeString(ProximiioOutput.OutputType.XHTML)) ||
                                type.equals(ProximiioOutput.getOutputTypeString(ProximiioOutput.OutputType.Push))) {
                            proximiio.outputs.add(new ProximiioOutput(entry.getKey(), next.get("content"), next.get("title"), type));
                        }
                    }

                    proximiio.resolveOutputs();
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                firebaseError.toException().printStackTrace();
            }
        };

        departmentListener = new ValueEventListener() {
            @SuppressWarnings("unchecked")
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();
                if (value != null) {
                    Set<Map.Entry<String, Object>> entrySet = value.entrySet();

                    //At first, none have been found in the current set of departments
                    for (int i = 0; i < proximiio.departments.size(); i++) {
                        proximiio.departments.get(i).setFound(false);
                    }

                    //Loop through all current departments
                    for (Map.Entry<String, Object> entry : entrySet) {
                        boolean found = false;
                        Map<String, Object> values = (Map<String, Object>)entry.getValue();
                        Firebase entryRef = firebase.child("organizations").child(proximiio.organization).child("departments").child(entry.getKey());

                        //Check if this department already exists locally
                        for (int i = 0; i < proximiio.departments.size(); i++) {
                            ProximiioDepartment department = proximiio.departments.get(i);
                            if (department.getID().equals(entry.getKey())) {
                                department.setFound(true);
                                department.setPlaceID((String)values.get("placeUID"));
                                department.setName((String)values.get("name"));
                                found = true;
                                break;
                            }
                        }

                        //If not, add it
                        if (!found) {
                            ProximiioDepartment department = new ProximiioDepartment(entryRef, (String)values.get("placeUID"), (String)values.get("name"));
                            proximiio.departments.add(department);
                            department.setFound(true);
                        }
                    }

                    //All departments that have not been found, have been deleted
                    for (int i = proximiio.departments.size() - 1; i >= 0; i--) {
                        ProximiioDepartment department = proximiio.departments.get(i);
                        if (!department.getFound()) {
                            department.destroy(false);
                            proximiio.departments.remove(i);
                        }
                    }

                    resolvePlaces();
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                firebaseError.toException().printStackTrace();
            }
        };

        settingsListener = new ValueEventListener() {
            @SuppressWarnings("unchecked")
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();
                if (value != null) {
                    String organizationId = (String) value.get("organization");
                    String type = (String) value.get("type");

                    if (organizationId != null) {
                        if (type.equals("mobile")) {
                            Map<String, Object> settings = (Map<String, Object>) value.get("settings");

                            String prevId = proximiio.organization;
                            proximiio.organization = organizationId;

                            //Get an anonymous ID
                            if (proximiio.proximiioID == null) {
                                proximiio.proximiioID = new ProximiioID(proximiio.activity, firebase, proximiio);
                            }

                            if (proximiio.organizationGeoFire == null) {
                                proximiio.organizationGeoFire = new GeoFire(firebase.child("organizations").child(proximiio.organization).child("geofire"));
                            }

                            boolean prevIBeacons = !proximiio.background ? proximiio.iBeaconsEnabled : proximiio.iBeaconsEnabledBackground;
                            boolean prevEddystones = !proximiio.background ? proximiio.eddystoneEnabled : proximiio.eddystoneEnabledBackground;
                            boolean prevIndoorAtlas = !proximiio.background ? proximiio.indoorAtlasEnabled : proximiio.indoorAtlasEnabledBackground;
                            boolean prevGPSGeofences = !proximiio.background ? proximiio.gpsGeofencesEnabled : proximiio.gpsGeofencesEnabledBackground;
                            boolean prevSteerPath = !proximiio.background ? proximiio.steerPathEnabled : proximiio.steerPathEnabledBackground;
                            String prevApiKey = proximiio.apiKey;
                            String prevApiSecret = proximiio.apiSecret;
                            String prevSteerPathNDD = proximiio.steerPathNDD;

                            if (settings.get("eddystone") != null) {
                                proximiio.eddystoneEnabled = (boolean) settings.get("eddystone");
                            }
                            proximiio.iBeaconsEnabled = (boolean) settings.get("ibeacons");
                            proximiio.indoorAtlasEnabled = (boolean) settings.get("indooratlas");
                            proximiio.gpsGeofencesEnabled = (boolean) settings.get("gpsgeofences");
                            proximiio.steerPathEnabled = (boolean) settings.get("steerpath");
                            Object temp = settings.get("ibeaconsbackground");
                            proximiio.iBeaconsEnabledBackground = temp != null ? (boolean)temp : proximiio.iBeaconsEnabled;
                            proximiio.iBeaconsEnabledBackground = temp != null ? (boolean)temp : proximiio.iBeaconsEnabled;
                            Object temp2 = settings.get("indooratlasbackground");
                            proximiio.indoorAtlasEnabledBackground = temp2 != null ? (boolean)temp2 : proximiio.indoorAtlasEnabled;
                            Object temp3 = settings.get("gpsgeofencesbackground");
                            proximiio.gpsGeofencesEnabledBackground = temp3 != null ? (boolean)temp3 : proximiio.gpsGeofencesEnabled;
                            Object temp4 = settings.get("steerpathbackground");
                            proximiio.steerPathEnabledBackground = temp4 != null ? (boolean)temp4 : proximiio.steerPathEnabled;
                            Object temp5 = settings.get("eddystonebackground");
                            proximiio.eddystoneEnabledBackground = temp5 != null ? (boolean)temp : proximiio.eddystoneEnabled;

                            proximiio.apiKey = (String) settings.get("indooratlasapikey");
                            if (proximiio.apiKey == null) {
                                proximiio.apiKey = "";
                            }
                            proximiio.apiSecret = (String) settings.get("indooratlasapikeysecret");
                            if (proximiio.apiSecret == null) {
                                proximiio.apiSecret = "";
                            }

                            proximiio.steerPathNDD = (String) settings.get("steerpathndd");

                            boolean newIBeacons = !proximiio.background ? proximiio.iBeaconsEnabled : proximiio.iBeaconsEnabledBackground;
                            boolean newEddystones = !proximiio.background ? proximiio.eddystoneEnabled : proximiio.eddystoneEnabledBackground;
                            boolean newIndoorAtlas = !proximiio.background ? proximiio.indoorAtlasEnabled : proximiio.indoorAtlasEnabledBackground;
                            boolean newGPSGeofences = !proximiio.background ? proximiio.gpsGeofencesEnabled : proximiio.gpsGeofencesEnabledBackground;
                            boolean newSteerPath = !proximiio.background ? proximiio.steerPathEnabled : proximiio.steerPathEnabledBackground;

                            boolean indoorAtlasApiKeyChanges = !proximiio.apiKey.equals(prevApiKey) ||
                                    !proximiio.apiSecret.equals(prevApiSecret);

                            boolean changes = prevIBeacons != newIBeacons || prevEddystones != newEddystones || prevIndoorAtlas != newIndoorAtlas ||
                                    prevGPSGeofences != newGPSGeofences || indoorAtlasApiKeyChanges || prevSteerPath != newSteerPath;

//                            if (proximiio.steerPath != null && !proximiio.steerPathNDD.equals(prevSteerPathNDD)) {
//                                proximiio.steerPath.setNDD(proximiio.steerPathNDD);
//                            }

                            Object tempForeground = settings.get("ibeaconinterval");
                            Object tempBackground = settings.get("ibeaconbackgroundinterval");

                            int iBeaconForeground = Math.max(Math.min(tempForeground != null ? (int)(long)tempForeground : Proximiio.iBeaconDefault, Proximiio.iBeaconMaxTiming), Proximiio.iBeaconMinTiming);
                            int iBeaconBackground = Math.max(Math.min(tempBackground != null ? (int)(long)tempBackground : Proximiio.iBeaconDefaultBackground, Proximiio.iBeaconMaxTiming), Proximiio.iBeaconMinTiming);

                            proximiio.iBeaconDuration = Math.min(iBeaconForeground / 2, Proximiio.iBeaconMaxDuration);
                            proximiio.iBeaconInterval = iBeaconForeground - proximiio.iBeaconDuration;
                            proximiio.iBeaconTimeout = iBeaconForeground * 3;
                            proximiio.iBeaconTimeoutInterval = iBeaconForeground * 2;

                            proximiio.iBeaconBackgroundDuration = Math.min(iBeaconBackground / 2, Proximiio.iBeaconMaxDuration);
                            proximiio.iBeaconBackgroundInterval = iBeaconBackground - proximiio.iBeaconBackgroundDuration;
                            proximiio.iBeaconBackgroundTimeout = iBeaconBackground * 3;
                            proximiio.iBeaconBackgroundTimeoutInterval = iBeaconBackground * 2;

                            if (!proximiio.background) {
                                ProximiioService.scanDuration = proximiio.iBeaconDuration;
                                ProximiioService.scanInterval = proximiio.iBeaconInterval;
                                ProximiioService.iBeaconTimeout = proximiio.iBeaconTimeout;
                                ProximiioService.iBeaconTimeoutInterval = proximiio.iBeaconTimeoutInterval;
                            }
                            else {
                                ProximiioService.scanDuration = proximiio.iBeaconBackgroundDuration;
                                ProximiioService.scanInterval = proximiio.iBeaconBackgroundInterval;
                                ProximiioService.iBeaconTimeout = proximiio.iBeaconBackgroundTimeout;
                                ProximiioService.iBeaconTimeoutInterval = proximiio.iBeaconBackgroundTimeoutInterval;
                            }

                            Object hint = settings.get("hintgpstoindooratlas");
                            proximiio.hintToIndoorAtlas = hint != null ? (boolean)hint : Proximiio.defaultHint;

                            int prevAccuracy = proximiio.background ? proximiio.gpsGeofenceBackgroundAccuracy : proximiio.gpsGeofenceAccuracy;
                            Object accuracy = settings.get("gpsgeofencesaccuracy");
                            proximiio.gpsGeofenceAccuracy = accuracy != null ? Math.max(Math.min((int)(long)accuracy, 3), 0) : Proximiio.gpsGeofenceDefaultAccuracy;
                            Object accuracy2 = settings.get("gpsgeofencesbackgroundaccuracy");
                            proximiio.gpsGeofenceBackgroundAccuracy = accuracy2 != null ? Math.max(Math.min((int)(long)accuracy2, 3), 0) : Proximiio.gpsGeofenceDefaultBackgroundAccuracy;
                            int newAccuracy = proximiio.background ? proximiio.gpsGeofenceBackgroundAccuracy : proximiio.gpsGeofenceAccuracy;

                            if (!proximiio.init || changes) {
                                if (proximiio.listener != null) {
                                    if (prevIBeacons != newIBeacons || !proximiio.init) {
                                        proximiio.listener.scanChange(ProximiioInput.InputType.iBeacon, newIBeacons);
                                    }
                                    if (prevEddystones != newEddystones || !proximiio.init) {
                                        proximiio.listener.scanChange(ProximiioInput.InputType.EddyStone, newEddystones);
                                    }
                                    if (prevIndoorAtlas != newIndoorAtlas || !proximiio.init) {
                                        proximiio.listener.scanChange(ProximiioInput.InputType.IndoorAtlas, newIndoorAtlas);
                                    }
                                    if (prevGPSGeofences != newGPSGeofences || !proximiio.init) {
                                        proximiio.listener.scanChange(ProximiioInput.InputType.GPSGeofence, newGPSGeofences);
                                    }
                                    if (prevSteerPath != newSteerPath || !proximiio.init) {
                                        proximiio.listener.scanChange(ProximiioInput.InputType.SteerPath, newSteerPath);
                                    }
                                }

                                if (proximiio.init && !organizationId.equals(prevId)) {
                                    proximiio.cleanOrganization(prevId);
                                    organizationData = null;
                                    proximiio.init = false;
                                }

                                if (!proximiio.init) {
                                    firebase.child("organizations").child(proximiio.organization).child("inputs").addValueEventListener(organizationListener);
                                    firebase.child("organizations").child(proximiio.organization).child("actionflows").addValueEventListener(actionFlowListener);
                                    firebase.child("organizations").child(proximiio.organization).child("outputs").addValueEventListener(outputListener);
                                    firebase.child("organizations").child(proximiio.organization).child("departments").addValueEventListener(departmentListener);
                                    firebase.child("organizations").child(proximiio.organization).child("floors").addValueEventListener(floorListener);
                                    firebase.child("organizations").child(proximiio.organization).child("places").addValueEventListener(placeListener);
                                }

                                if (proximiio.listener != null && (!organizationId.equals(prevId) || indoorAtlasApiKeyChanges)) {
                                    proximiio.listener.apiKeyChange(proximiio.organization, proximiio.proximiioID.getId(), proximiio.apiKey, proximiio.apiSecret);
                                }

                                boolean noInit = !proximiio.init;
                                proximiio.init = true;
                                proximiio.setUpOrganization(noInit || prevIBeacons != newIBeacons, noInit || prevEddystones != newEddystones, noInit || prevIndoorAtlas != newIndoorAtlas ||
                                        indoorAtlasApiKeyChanges, noInit || prevGPSGeofences != newGPSGeofences, noInit || prevSteerPath != newSteerPath, true);

                                if (!noInit && prevGPSGeofences == newGPSGeofences && ((!proximiio.background && proximiio.gpsGeofencesEnabled) || (proximiio.background && proximiio.gpsGeofencesEnabledBackground)) && prevAccuracy != newAccuracy) {
                                    proximiio.geofence.setAccuracy(newAccuracy);
                                    proximiio.geofence.stopUpdates();
                                    proximiio.geofence.startUpdates();
                                }
                            }
                        } else {
                            proximiio.toast("Application type is not mobile!");
                            if (proximiio.listener != null) {
                                proximiio.listener.error(ProximiioListener.Error.NotMobile);
                            }
                        }
                    } else {
                        proximiio.toast("Organization ID not found in app settings! (Most likely invalid App ID)");
                        if (proximiio.listener != null) {
                            proximiio.listener.error(ProximiioListener.Error.InvalidAppID);
                        }
                    }
                } else {
                    proximiio.toast("App key was invalid!");
                    if (proximiio.listener != null) {
                        proximiio.listener.error(ProximiioListener.Error.InvalidAppID);
                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                firebaseError.toException().printStackTrace();
            }
        };
    }

    protected void init(String authToken) {
//        System.out.println("initializing firebase, authToken: " + authToken);
        if (error) {
            error = false;
            firebase.authWithCustomToken(authToken, new Firebase.AuthResultHandler() {
                @Override
                public void onAuthenticated(AuthData authData) {
                    if (proximiio.appID != null) {
                        firebase.child("applications").child(proximiio.appID).addValueEventListener(settingsListener);
                    }
                }

                @Override
                public void onAuthenticationError(FirebaseError firebaseError) {
                    System.out.println("authError" + firebaseError.getMessage() + " code:" + firebaseError.getCode())  ;
                    error = true;
                    firebaseError.toException().printStackTrace();
                    if (proximiio.listener != null) {
                        proximiio.listener.error(ProximiioListener.Error.InvalidToken);
                    }
                }
            });
        }
    }

    private void resolvePlaces() {
        for (int i = 0; i < proximiio.departments.size(); i++) {
            ProximiioDepartment department = proximiio.departments.get(i);
            for (int j = 0; j < places.size(); j++) {
                ProximiioPlace place = places.get(j);
                if (department.getPlaceID().equals(place.getID())) {
                    department.setPlace(place);
                    break;
                }
            }
        }
    }
}
