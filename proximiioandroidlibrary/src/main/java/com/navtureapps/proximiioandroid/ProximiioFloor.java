package com.navtureapps.proximiioandroid;

public class ProximiioFloor {
    private String floorId;
    private String floorPlanId;

    protected String getFloorId() {
        return floorId;
    }

    protected String getFloorPlanId() {
        return floorPlanId;
    }

    public ProximiioFloor(String floorId, String floorPlanId) {
        this.floorId = floorId;
        this.floorPlanId = floorPlanId;
    }
}
