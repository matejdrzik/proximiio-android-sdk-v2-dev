package com.navtureapps.proximiioandroid;

import java.util.ArrayList;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingApi;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class ProximiioGeofence implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener {
    private ArrayList<Geofence> toAdd;
    private GeofencingApi geofencing;
    private GoogleApiClient apiClient;
    private PendingIntent pendingIntent;
    private ArrayList<ProximiioInput> inputs;
    private ArrayList<Message> messageQueue;
    private Messenger messenger;
    private ServiceConnection serviceConnection;
    private ProximiioGeofenceReceiver receiver;
    private Proximiio proximiio;
    private LocationRequest locationRequest;
    private Activity activity;
    private Location lastLocation;
    private boolean removeAll;
    private boolean tracking;
    private boolean queueStart;
    private boolean queueStop;
    private static String tag = "ProximiioGeofencing";

    protected void setProximiio(Proximiio proximiio) {
        this.proximiio = proximiio;
    }

    protected ArrayList<ProximiioInput> getInputs() {
        return inputs;
    }

    protected ProximiioGeofence(Activity activity, final Proximiio proximiio) {
        this.activity = activity;
        this.proximiio = proximiio;
        removeAll = false;
        tracking = false;
        queueStart = false;
        queueStop = false;
        inputs = new ArrayList<>();
        toAdd = new ArrayList<>();
        geofencing = LocationServices.GeofencingApi;
        Intent serviceIntent = new Intent(activity, ProximiioGeofenceService.class);
        pendingIntent = PendingIntent.getService(activity, 0, serviceIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        apiClient = new GoogleApiClient.Builder(activity)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        apiClient.connect();

        receiver = new ProximiioGeofenceReceiver() {
            @Override
            public void enteringGeofence(Geofence geofence, Location location) {
                checkEvent(geofence, location, true);
            }

            @Override
            public void exitingGeofence(Geofence geofence, Location location) {
                checkEvent(geofence, location, false);
            }

            @Override
            public String getReceiverID() {
                return "ProximiioGeofence";
            }
        };

        messageQueue = new ArrayList<>();
        serviceConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName className, IBinder service) {
                messenger = new Messenger(service);
                sendMessage(messenger, ProximiioGeofenceService.REGISTER_CLIENT, receiver);
                
                for (int i = 0; i < messageQueue.size(); i++) {
                    sendMessage(messenger, messageQueue.get(i));
                }
                messageQueue.clear();
            }

            //Service crashed
            public void onServiceDisconnected(ComponentName className) {
                messenger = null;
            }
        };

        activity.bindService(new Intent(activity, ProximiioGeofenceService.class), serviceConnection, Context.BIND_AUTO_CREATE);
    }

    private void checkEvent(Geofence geofence, Location location, boolean enter) {
        for (int i = 0; i < inputs.size(); i++) {
            ProximiioInput input = inputs.get(i);
            if (input.getId().equals(geofence.getRequestId())) {
                input.setAccuracy(location.getAccuracy());
                proximiio.generateEvent(input, enter);
                break;
            }
        }
    }

    private void sendMessage(Messenger messenger, int type, Object obj) {
        sendMessage(messenger, Message.obtain(null, type, obj));
    }

    private void sendMessage(Messenger messenger, Message message) {
        try {
            if (messenger != null) {
                messenger.send(message);
            }
            else {
                messageQueue.add(message);
            }
        }
        catch (RemoteException e) {
            messageQueue.add(message);
        }
    }

    protected void setAccuracy(int accuracy) {
        locationRequest = new LocationRequest();
        switch (accuracy) {
            case 0:
                locationRequest.setPriority(LocationRequest.PRIORITY_NO_POWER);
                locationRequest.setInterval(120000);
                locationRequest.setFastestInterval(20000);
                break;
            case 1:
                locationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
                locationRequest.setInterval(60000);
                locationRequest.setFastestInterval(15000);
                break;
            default:
            case 2:
                locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
                locationRequest.setInterval(30000);
                locationRequest.setFastestInterval(10000);
                break;
            case 3:
                locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                locationRequest.setInterval(15000);
                locationRequest.setFastestInterval(5000); //5000 to guarantee accuracy?
                break;
        }
    }

    protected void addGeofence(ProximiioInput input) {
        Geofence geofence = new Geofence.Builder()
                .setRequestId(input.getId())
                .setCircularRegion(input.getLat(), input.getLon(), (float)input.getRadius())
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                .build();

        inputs.add(input);

        startUpdates();

        addGeofence(geofence);
    }

    protected void addGeofence(Geofence geofence) {
        if (apiClient.isConnected()) {
            GeofencingRequest geofencingRequest = new GeofencingRequest.Builder()
                    .addGeofence(geofence)
                    .setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)
                    .build();
            geofencing.addGeofences(apiClient, geofencingRequest, pendingIntent);
            Log.d(tag, "Geofence added: " + geofence.toString());
        }
        else {
            toAdd.add(geofence);
        }
    }

    protected void removeGeofences() {
        if (!inputs.isEmpty()) {
            stopUpdates();
            if (apiClient.isConnected()) {
                removeAll = false;
                geofencing.removeGeofences(apiClient, pendingIntent);
            } else {
                removeAll = true;
            }

            for (int i = 0; i < inputs.size(); i++) {
                ProximiioInput input = inputs.get(i);
                if (input != null && input.getEntered()) {
                    Log.d("ProximiioGeofence", "input" + input);
                    Log.d("ProximiioGeofence", "lastLocation" + lastLocation);
                    if (lastLocation != null) {
                        input.setAccuracy(lastLocation.getAccuracy());
                        proximiio.generateEvent(input, false);
                    }
                }
            }

            inputs.clear();
            toAdd.clear();
        }
    }

    protected void startUpdates() {
        if (apiClient.isConnected()) {
            if (!tracking) {
                LocationServices.FusedLocationApi.requestLocationUpdates(apiClient, locationRequest, this);
                tracking = true;
            }
        }
        else {
            queueStop = false;
            queueStart = true;
        }
    }

    protected void stopUpdates() {
        if (apiClient.isConnected()) {
            if (tracking) {
                LocationServices.FusedLocationApi.removeLocationUpdates(apiClient, this);
                tracking = false;
            }
        }
        else {
            queueStart = false;
            queueStop = true;
        }
    }

    protected void destroy(boolean background) {
        if (!background) {
            removeGeofences();
            apiClient.disconnect();
        }
        if (messenger != null) {
            if (!background) {
                sendMessage(messenger, ProximiioGeofenceService.UNREGISTER_CLIENT, receiver);
                sendMessage(messenger, ProximiioGeofenceService.STOP, null);
            }
            try {
                activity.unbindService(serviceConnection);
            }
            catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
            messenger = null;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        lastLocation = location;
        proximiio.updatePositions(location.getLatitude(), location.getLongitude(), location.getAccuracy(), ProximiioInput.InputType.GPSGeofence);
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.w(tag, "Connection failed!");
        apiClient.reconnect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(tag, "Connected!");
        //Log.d(tag, "Location: " + LocationServices.FusedLocationApi.getLastLocation(apiClient).toString());
        if (removeAll) {
            geofencing.removeGeofences(apiClient, pendingIntent);
            removeAll = false;
        }

        for (int i = toAdd.size() - 1; i >= 0; i--) {
            addGeofence(toAdd.get(i));
            toAdd.remove(i);
        }

        if (queueStart) {
            startUpdates();
            queueStart = false;
        }
        else if (queueStop) {
            stopUpdates();
            queueStop = false;
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.w(tag, "Connection suspended!");
    }
}
