//Copyright � 2015 NavtureApps (http://navtureapps.com/)

package com.navtureapps.proximiioandroid;

import android.location.Location;

import com.google.android.gms.location.Geofence;

/**
 * Use this to track geofences if needed. Add this to the geofencing in progress
 * with {@link com.navtureapps.proximiiosdk.ProximiioAPI#addGeofenceReceiver(ProximiioGeofenceReceiver)}.
 */
public interface ProximiioGeofenceReceiver {

	/**
	 * Called when the user enters a geofence.
	 * 
	 * @param geofence
	 *            The geofence in question.
	 * @param triggerPoint
	 *            The point at which this event was triggered.
	 */
	public void enteringGeofence(Geofence geofence, Location triggerPoint);

	/**
	 * Called when the user leaves a geofence.
	 * 
	 * @param geofence
	 *            The geofence in question.
	 * @param triggerPoint
	 *            The point at which this event was triggered.
	 */
	public void exitingGeofence(Geofence geofence, Location triggerPoint);

	/**
	 * This should return a unique yet consistent ID for an implementation (for
	 * example, the application package identifier). Every time an interface is
	 * hooked up, the service checks if that interface is already being served.
	 * This implementation doesn't require the API user to check if the service
	 * is already hooked up, and makes it safe to create an instance of the API
	 * class on every Activity's onCreate. If you want to run multiple
	 * listeners, use different IDs for them or they will be considered the
	 * same, and all but one will be dropped.
	 * 
	 * @return This implementation's ID.
	 */
	public abstract String getReceiverID();
}
