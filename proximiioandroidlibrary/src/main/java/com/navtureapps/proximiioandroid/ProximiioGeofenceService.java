package com.navtureapps.proximiioandroid;

import android.app.IntentService;
import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class ProximiioGeofenceService extends IntentService {
    private ArrayList<ProximiioGeofenceReceiver> clients;

    private final static String tag = "ProximiioGeoService";

    private final Messenger messenger = new Messenger(new IncomingHandler(this));

    private enum eventType {
        ENTERING,
        EXITING
    }

    public static final int REGISTER_CLIENT = 0;
    public static final int UNREGISTER_CLIENT = 1;
    public static final int STOP = 2;

    private static class IncomingHandler extends Handler {
        private final WeakReference<ProximiioGeofenceService> proximiioGeofenceService;

        IncomingHandler(ProximiioGeofenceService proximiioGeofenceService) {
            this.proximiioGeofenceService = new WeakReference<>(proximiioGeofenceService);
        }

        @Override
        public void handleMessage(Message msg) {
            ProximiioGeofenceService service = proximiioGeofenceService.get();
            switch (msg.what) {
                case ProximiioGeofenceService.REGISTER_CLIENT:
                    ProximiioGeofenceReceiver receiver = (ProximiioGeofenceReceiver) msg.obj;
                    if (!service.getExistingClient(receiver)) {
                        service.clients.add(receiver);
                        Log.d(ProximiioGeofenceService.tag, "Receiver registered.");
                    }
                    else {
                        Log.w(ProximiioGeofenceService.tag, "Receiver already registered.");
                    }
                    break;
                case ProximiioGeofenceService.UNREGISTER_CLIENT:
                    ProximiioGeofenceReceiver receiver2 = (ProximiioGeofenceReceiver) msg.obj;
                    if (service.clients.remove(receiver2)) {
                        Log.d(ProximiioGeofenceService.tag, "Receiver unregistered.");
                    }
                    else {
                        Log.w(ProximiioGeofenceService.tag, "Receiver was not registered when it was passed to be unregistered!");
                    }
                    break;
                case ProximiioGeofenceService.STOP:
                    service.stopSelf();
                    break;

            }
        }
    }

    public ProximiioGeofenceService() {
        super(tag);
        clients = new ArrayList<>();
    }

    @Override
    public void onHandleIntent(Intent intent) {
        GeofencingEvent event = GeofencingEvent.fromIntent(intent);
        if (event.hasError()) {
            Log.e(tag, "Error code: " + String.valueOf(event.getErrorCode()));
        }
        else {
            Location triggerPoint = event.getTriggeringLocation();
            switch (event.getGeofenceTransition()) {
                case Geofence.GEOFENCE_TRANSITION_ENTER:
                    Log.d(tag, "Enter: " + triggerPoint.toString());
                    eventForGeofence(event, triggerPoint, eventType.ENTERING);
                    break;
                case Geofence.GEOFENCE_TRANSITION_EXIT:
                    Log.d(tag, "Exit: " + triggerPoint.toString());
                    eventForGeofence(event, triggerPoint, eventType.EXITING);
                    break;
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return messenger.getBinder();
    }

    private void eventForGeofence(GeofencingEvent event, Location triggerPoint, eventType type) {
        List<Geofence> geofences = event.getTriggeringGeofences();
        for (int i = 0; i < geofences.size(); i++) {
            Geofence geofence = geofences.get(i);
            for (int j = 0; j < clients.size(); j++) {
                ProximiioGeofenceReceiver receiver = clients.get(j);
                switch (type) {
                    case ENTERING:
                        receiver.enteringGeofence(geofence, triggerPoint);
                        break;
                    case EXITING:
                        receiver.exitingGeofence(geofence, triggerPoint);
                        break;
                }
            }
        }
    }

    private boolean getExistingClient(ProximiioGeofenceReceiver receiver) {
        //Compare to all the existing receivers to see if we already have this one
        for (int i = 0; i < clients.size(); i++) {
            if (clients.get(i).getReceiverID().equals(receiver.getReceiverID())) {
                return true;
            }
        }
        return false;
    }
}
