//Copyright � 2014 NavtureApps (http://navtureapps.com/)

package com.navtureapps.proximiioandroid;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingApi;
import com.google.android.gms.location.GeofencingEvent;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;

public class ProximiioGeofencing extends BroadcastReceiver implements ConnectionCallbacks, OnConnectionFailedListener {
	private ArrayList<Geofence> toAdd;
	private ArrayList<ProximiioGeofenceReceiver> receivers;
	private GeofencingApi geofencing;
	private GoogleApiClient apiClient;
	private PendingIntent pendingIntent;
	private Intent serviceIntent;

	private static String tag = "ProximiioGeofencing";

	public static ArrayList<Geofence> geofences;
	
	private enum eventType {
		ENTERING,
		EXITING
	}

	public ProximiioGeofencing(Activity activity) {
		if (geofences == null) {
			geofences = new ArrayList<Geofence>();
		}
		toAdd = new ArrayList<Geofence>();
		receivers = new ArrayList<ProximiioGeofenceReceiver>();
		
		geofencing = LocationServices.GeofencingApi;
		serviceIntent = new Intent(activity, ProximiioGeofencing.class);
		pendingIntent = PendingIntent.getBroadcast(activity, 0, serviceIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		apiClient = new GoogleApiClient.Builder(activity)
				.addApi(LocationServices.API)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.build();
		apiClient.connect();
	}

	public Intent getServiceIntent() {
		return serviceIntent;
	}

	public void addGeofence(Geofence geofence) {
		if (apiClient.isConnected()) {
			geofences.add(geofence);
			GeofencingRequest geofencingRequest = new GeofencingRequest.Builder()
					.addGeofence(geofence)
					.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER | GeofencingRequest.INITIAL_TRIGGER_EXIT)
					.build();
			geofencing.addGeofences(apiClient, geofencingRequest, pendingIntent);
			Log.d(tag, "Geofence added: " + geofence.toString());
		} else {
			toAdd.add(geofence);
		}
	}

	public void removeGeofence(Geofence geofence) {
		ArrayList<String> strings = new ArrayList<String>();
		strings.add(geofence.getRequestId());
		geofencing.removeGeofences(apiClient, strings);
		geofences.remove(geofence);
		toAdd.remove(geofence);
	}

	public void destroy() {
		apiClient.disconnect();
	}
	
	public void addReceiver(ProximiioGeofenceReceiver receiver) {
		if (!getExistingClient(receiver)) {
			receivers.add(receiver);
			Log.d(tag, "Receiver registered.");
		}
		else {
			Log.w(tag, "Receiver already registered!");
		}
	}
	
	public void removeReceiver(ProximiioGeofenceReceiver receiver) {
		if (receivers.remove(receiver)) {
			Log.d(tag, "Receiver unregistered.");
		}
		else {
			Log.w(tag, "Receiver sent to be unregistered was not found!");
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		Log.w(tag, "Connection failed!");
		apiClient.reconnect();
	}

	@Override
	public void onConnected(Bundle bundle) {
		Log.d(tag, "Connected!");
		Log.d(tag, "Location: " + LocationServices.FusedLocationApi.getLastLocation(apiClient).toString());
		for (int i = toAdd.size() - 1; i >= 0; i--) {
			addGeofence(toAdd.get(i));
			toAdd.remove(i);
		}
	}

	@Override
	public void onConnectionSuspended(int i) {
		Log.w(tag, "Connection suspended!");
	}
	
	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d(tag, "WE GOT SOMETHING!");
		GeofencingEvent event = GeofencingEvent.fromIntent(intent);
		if (event.hasError()) {
			Log.e(tag, "Error code: " + String.valueOf(event.getErrorCode()));
		} else {
			Location triggerPoint = event.getTriggeringLocation();
			switch (event.getGeofenceTransition()) {
			case Geofence.GEOFENCE_TRANSITION_ENTER:
				Log.d(tag, "Enter: " + triggerPoint.toString());
				eventForGeofence(event, triggerPoint, eventType.ENTERING, context);
				break;
			case Geofence.GEOFENCE_TRANSITION_EXIT:
				Log.d(tag, "Exit: " + triggerPoint.toString());
				eventForGeofence(event, triggerPoint, eventType.EXITING, context);
				break;
			}
		}
	}

	private void eventForGeofence(GeofencingEvent event, Location triggerPoint, eventType type, Context context) {
		List<Geofence> geofences = event.getTriggeringGeofences();
		for (int i = 0; i < geofences.size(); i++) {
			Geofence geofence = geofences.get(i);
			for (int j = 0; j < receivers.size(); j++) {
				ProximiioGeofenceReceiver receiver = receivers.get(j);
				switch (type) {
				case ENTERING:
					receiver.enteringGeofence(geofence, triggerPoint);
					break;
				case EXITING:
					receiver.exitingGeofence(geofence, triggerPoint);
					break;
				}
			}
		}
	}
	
	private boolean getExistingClient(ProximiioGeofenceReceiver receiver) {
		// Compare to all the existing receivers to see if we already have this one
		for (int i = 0; i < receivers.size(); i++) {
			if (receivers.get(i).getReceiverID().equals(receiver.getReceiverID())) {
				return true;
			}
		}
		return false;
	}
}
