package com.navtureapps.proximiioandroid;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.util.Log;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ServerValue;
import com.firebase.client.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class ProximiioID {

    private static final String TAG = "ProximiioID";
    private Firebase firebase;
    private String id;
    private HashMap<String, Long> departments;
    private HashMap<String, Long> places;
    private Proximiio proximiio;
    private boolean init;

    protected void setProximiio(Proximiio proximiio) {
        this.proximiio = proximiio;
    }

    protected boolean finishedInit() {
        return init;
    }

    protected ProximiioID(Activity activity, Firebase firebase, Proximiio proximiio) {
        this.firebase = firebase;
        this.proximiio = proximiio;
        places = new HashMap<>();
        departments = new HashMap<>();
        init = false;

        SharedPreferences preferences = activity.getSharedPreferences("ProximiioID", Context.MODE_PRIVATE);
        String id = preferences.getString("ID", null);
        if (id == null) {
            Firebase ano = firebase.child("organizations").child(proximiio.organization).child("anonymousVisitors").push();
            id = ano.getKey();
            ano.child("created").setValue(ServerValue.TIMESTAMP);
            Firebase data = ano.child("androidData");
            Configuration config = activity.getResources().getConfiguration();
            data.child("version").setValue(Build.VERSION.RELEASE);
            data.child("board").setValue(Build.BOARD);
            data.child("bootloader").setValue(Build.BOOTLOADER);
            data.child("brand").setValue(Build.BRAND);
            data.child("densityDPI").setValue(config.densityDpi);
            data.child("device").setValue(Build.DEVICE);
            data.child("display").setValue(Build.DISPLAY);
            data.child("fingerprint").setValue(Build.FINGERPRINT);
            data.child("fontScale").setValue(config.fontScale);
            data.child("hardKeyboardHidden").setValue(config.hardKeyboardHidden);
            data.child("hardware").setValue(Build.HARDWARE);
            data.child("host").setValue(Build.HOST);
            data.child("id").setValue(Build.ID);
            data.child("keyboard").setValue(config.keyboard);
            data.child("keyboardHidden").setValue(config.keyboardHidden);
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                data.child("locale").setValue(config.locale.getLanguage());
                ano.child("locale").setValue(config.locale.getLanguage());
            }
            else {
                data.child("locale").setValue(config.locale.toLanguageTag());
                ano.child("locale").setValue(config.locale.toLanguageTag());
            }
            data.child("localeData").setValue(config.locale.toString());
            data.child("manufacturer").setValue(Build.MANUFACTURER);
            data.child("mcc").setValue(config.mcc);
            data.child("mnc").setValue(config.mnc);
            data.child("model").setValue(Build.MODEL);
            data.child("navigation").setValue(config.navigation);
            data.child("navigationHidden").setValue(config.navigationHidden);
            data.child("orientation").setValue(config.orientation);
            data.child("os").setValue("Android");
            ano.child("os").setValue("Android");
            data.child("product").setValue(Build.PRODUCT);
            data.child("radio").setValue(Build.getRadioVersion());
            data.child("screenHeightDP").setValue(config.screenHeightDp);
            data.child("screenLayout").setValue(config.screenLayout);
            data.child("screenWidthDP").setValue(config.screenWidthDp);
            data.child("serial").setValue(Build.SERIAL);
            data.child("smallestScreenWidthDP").setValue(config.smallestScreenWidthDp);
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                data.child("supportedABIs").setValue(new String[] { Build.CPU_ABI });
            }
            else {
                data.child("supportedABIs").setValue(Build.SUPPORTED_ABIS);
            }
            data.child("tags").setValue(Build.TAGS);
            data.child("time").setValue(Build.TIME);
            data.child("touchscreen").setValue(config.touchscreen);
            data.child("type").setValue(Build.TYPE);
            data.child("uiMode").setValue(config.uiMode);
            data.child("user").setValue(Build.USER);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("ID", id);
            editor.apply();
        }
        this.id = id;

        firebase.child("organizations").child(proximiio.organization).child("anonymousVisitors").child(id).addListenerForSingleValueEvent(new ValueEventListener() {
            @SuppressWarnings("unchecked")
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();
                if (value != null) {
                    for (Map.Entry<String, Object> entry : value.entrySet()) {
                        if (entry.getKey().equals("places")) {
                            Map<String, Object> next = (Map<String, Object>)entry.getValue();
                            for (Map.Entry<String, Object> place : next.entrySet()) {
                                places.put(place.getKey(), (Long)place.getValue());
                            }
                        }
                        else if (entry.getKey().equals("departments")) {
                            Map<String, Object> next = (Map<String, Object>)entry.getValue();
                            for (Map.Entry<String, Object> department : next.entrySet()) {
                                departments.put(department.getKey(), (Long) department.getValue());
                            }
                        }
                    }
                }
                init = true;
            }

            @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
            @Override
            public void onCancelled(FirebaseError firebaseError) {
                firebaseError.toException().printStackTrace();
            }
        });
    }

    protected String getId() {
        return id;
    }

    protected boolean returningToDepartment(String departmentID) {
        return returningTo("departments", departments, departmentID);
    }

    protected boolean returningToPlace(String placeID) {
        return returningTo("places", places, placeID);
    }

    private boolean returningTo(String firebaseKey, HashMap<String, Long> values, String id) {
        Long value = values.get(id);
        boolean result = value != null;
        value = result ? value + 1 : 1L;
        values.put(id, value);
        firebase.child("organizations").child(proximiio.organization).child("anonymousVisitors").child(this.id).child(firebaseKey).child(id).setValue(value);
        return result;
    }
}
