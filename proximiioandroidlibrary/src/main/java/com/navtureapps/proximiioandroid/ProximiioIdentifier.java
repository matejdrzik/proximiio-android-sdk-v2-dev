//Copyright � 2014 NavtureApps (http://navtureapps.com/)

package com.navtureapps.proximiioandroid;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Gets a unique ID for this device by SHA1-hashing a combination of the
 * device's Bluetooth MAC-address and a random byte sequence. Stores the ID in
 * the application's preferences.
 */
public class ProximiioIdentifier {
	private static String id;
	private static HashMap<String, String> userInfo;
	private static HashMap<String, String> eventInfo;
	private static SharedPreferences preferences;

	/**
	 * Add or modify user info. User info is sent along with each iBeacon status
	 * update.
	 * 
	 * @param key
	 *            The key for the mapping, e.g. "name".
	 * @param value
	 *            The value for the specified key.
	 * @param activity
	 */
	public static void setUserInfo(String key, String value, Activity activity) {
		userInfo = checkInfo(activity, userInfo, "user");
		userInfo.put(key, value);
		saveChanges(userInfo, "user");
	}

	/**
	 * Remove a previously added key and its' value from user info.
	 * 
	 * @param key
	 *            The key you want to remove.
	 * @param activity
	 */
	public static void removeUserInfo(String key, Activity activity) {
		userInfo = checkInfo(activity, userInfo, "user");
		if (userInfo.remove(key) != null) {
			saveChanges(userInfo, "user");
		}
	}

	/**
	 * Get all user info entries as key-value pairs.
	 * 
	 * @param activity
	 * @return Key-value pairs of all user info entries.
	 */
	public static Iterator<Entry<String, String>> getUserInfoIterator(
			Activity activity) {
		userInfo = checkInfo(activity, userInfo, "user");
		return userInfo.entrySet().iterator();
	}

	/**
	 * Get a user info value with a key.
	 * 
	 * @param key
	 *            The key to search with.
	 * @param activity
	 * @return The value if found, otherwise null.
	 */
	public static String getUserInfo(String key, Activity activity) {
		userInfo = checkInfo(activity, userInfo, "user");
		return userInfo.get(key);
	}

	/**
	 * Add or modify event info. Event info is sent along with each iBeacon
	 * status update.
	 * 
	 * @param key
	 *            The key for the mapping, e.g. "name".
	 * @param value
	 *            The value for the specified key.
	 * @param activity
	 */
	public static void setEventInfo(String key, String value, Activity activity) {
		eventInfo = checkInfo(activity, eventInfo, "event");
		eventInfo.put(key, value);
		saveChanges(eventInfo, "event");
	}

	/**
	 * Remove a previously added key and its' value from event info.
	 * 
	 * @param key
	 *            The key you want to remove.
	 * @param activity
	 */
	public static void removeEventInfo(String key, Activity activity) {
		eventInfo = checkInfo(activity, eventInfo, "event");
		if (eventInfo.remove(key) != null) {
			saveChanges(eventInfo, "event");
		}
	}

	/**
	 * Get all event info entries as key-value pairs.
	 * 
	 * @param activity
	 * @return Key-value pairs of all event info entries.
	 */
	public static Iterator<Entry<String, String>> getEventInfoIterator(
			Activity activity) {
		eventInfo = checkInfo(activity, eventInfo, "event");
		return eventInfo.entrySet().iterator();
	}

	/**
	 * Get an event info value with a key.
	 * 
	 * @param key
	 *            The key to search with.
	 * @param activity
	 * @return The value if found, otherwise null.
	 */
	public static String getEventInfo(String key, Activity activity) {
		eventInfo = checkInfo(activity, eventInfo, "event");
		return eventInfo.get(key);
	}

	private static HashMap<String, String> checkInfo(Activity activity,
			HashMap<String, String> hashMap, String key) {
		if (hashMap == null) {
			checkPreferences(activity);
			hashMap = new HashMap<String, String>();
			Set<String> keys = preferences.getStringSet(key, null);
			if (keys == null) {
				saveChanges(hashMap, key);
			} else {
				Iterator<String> iter = keys.iterator();
				while (iter.hasNext()) {
					String[] saved = iter.next().split(":");
					if (saved.length == 2) {
						hashMap.put(saved[0], saved[1]);
					} else {
						hashMap.put(saved[0], "");
					}
				}
			}
		}
		return hashMap;
	}

	private static void checkPreferences(Activity activity) {
		if (preferences == null) {
			preferences = activity.getSharedPreferences("ProximiioIdentifier",
					Context.MODE_PRIVATE);
		}
	}

	private static void saveChanges(HashMap<String, String> hashMap, String key) {
		Set<String> keys = new HashSet<String>();
		Iterator<Entry<String, String>> iter = hashMap.entrySet().iterator();

		while (iter.hasNext()) {
			Entry<String, String> entry = iter.next();
			keys.add(entry.getKey() + ":" + entry.getValue());
		}

		SharedPreferences.Editor editor = preferences.edit();
		editor.putStringSet(key, keys);
		editor.apply();
	}

	protected static String getID(Activity activity) {
		if (id == null) {
			checkPreferences(activity);

			String idCandidate = preferences.getString("id", "null");
			if (idCandidate.equals("null")) {
				MessageDigest sha = null;
				try {
					sha = MessageDigest.getInstance("SHA-1");

				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				}

				byte[] mac = BluetoothAdapter.getDefaultAdapter().getAddress()
						.getBytes(Charset.forName("UTF-8"));

				byte[] salt = new byte[64];
				new Random().nextBytes(salt);

				byte[] input = concatArray(mac, salt);

				id = hexEncode(sha.digest(input));

				SharedPreferences.Editor editor = preferences.edit();
				editor.putString("id", id);
				editor.apply();
			} else {
				id = idCandidate;
			}
			Log.d("ProximiioIdentifier", "This device's ID is " + id);
		}
		return id;
	}

	private static byte[] concatArray(byte[] A, byte[] B) {
		int aLen = A.length;
		int bLen = B.length;
		byte[] C = new byte[aLen + bLen];
		System.arraycopy(A, 0, C, 0, aLen);
		System.arraycopy(B, 0, C, aLen, bLen);
		return C;
	}

	private static String hexEncode(byte[] aInput) {
		StringBuilder result = new StringBuilder();
		char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				'a', 'b', 'c', 'd', 'e', 'f' };
		for (int idx = 0; idx < aInput.length; ++idx) {
			byte b = aInput[idx];
			result.append(digits[(b & 0xf0) >> 4]);
			result.append(digits[b & 0x0f]);
		}
		return result.toString();
	}
}
