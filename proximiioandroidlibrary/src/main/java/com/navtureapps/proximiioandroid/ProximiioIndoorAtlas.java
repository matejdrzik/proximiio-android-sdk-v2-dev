package com.navtureapps.proximiioandroid;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.indooratlas.android.sdk.IALocation;
import com.indooratlas.android.sdk.IALocationListener;
import com.indooratlas.android.sdk.IALocationManager;
import com.indooratlas.android.sdk.IALocationRequest;
import com.indooratlas.android.sdk.IARegion;
import com.indooratlas.android.sdk.resources.IAResourceManager;

public class ProximiioIndoorAtlas implements IALocationListener {
    private static final String TAG = "ProximiioIndoorAtlas" ;
    private String venueId;
    private String floorId;
    private String floorPlanId;
    private int delay;
    private long lastUpdate;
    private ProximiioListener listener;
    private Proximiio proximiio;
    private boolean maxAccuracy;

    private IALocationManager mIALocationManager;
    private IALocation mLastLocation;

    protected void setProximiio(Proximiio proximiio) {
        this.proximiio = proximiio;
    }

    protected ProximiioIndoorAtlas(Activity activity, Proximiio proximiio, ProximiioListener listener, String apiKey, String apiSecret, String venueId, String floorId, String floorPlanId) {
        this.venueId = venueId;
        this.floorId = floorId;
        this.floorPlanId = floorPlanId;
        this.proximiio = proximiio;
        this.listener = listener;
        maxAccuracy = false;
        delay = 15000;
        Bundle extras = new Bundle(2);
        extras.putString(IALocationManager.EXTRA_API_KEY, apiKey);
        extras.putString(IALocationManager.EXTRA_API_SECRET, apiSecret);
        mIALocationManager = IALocationManager.create(activity, extras);
        startPositioning();
    }

    protected void setListener(ProximiioListener listener) {
        this.listener = listener;
        if (listener != null && maxAccuracy) {
            listener.indoorAtlasAccurate();
        }
    }

    protected void hintPosition(double lat, double lon, float accuracy) {
        IALocation location = new IALocation.Builder()
                .withLatitude(lat)
                .withLongitude(lon)
                .withAccuracy(accuracy).build();
        mIALocationManager.setLocation(location);
    }

    private void startPositioning() {
        Log.d(TAG, "IA START POSITIONING");
        IARegion.Listener regionListener = new IARegion.Listener() {
            @Override
            public void onEnterRegion(IARegion iaRegion) {
                Log.d(TAG, "onEnterRegion:" + iaRegion.toString());
            }

            @Override
            public void onExitRegion(IARegion iaRegion) {
                Log.d(TAG, "onExitRegion:" + iaRegion.toString());
            }
        };
        mIALocationManager.registerRegionListener(regionListener);
        mIALocationManager.requestLocationUpdates(IALocationRequest.create(), this);
        Log.d(TAG, "IA START POSITIONING");
    }

    protected void destroy() {
        mIALocationManager.removeLocationUpdates(this);
        if (mLastLocation != null) {
            proximiio.indoorAtlasInput(mLastLocation, true);
        }
        if (mIALocationManager != null) {
            mIALocationManager.destroy();
        }
    }


//    @Override
//    public void onServiceUpdate(ServiceState state) {
//        lastState = state;
//        proximiio.indoorAtlasInput(state, false);
//        long now = System.nanoTime();
//        if ((now - lastUpdate) / 1000000 > delay) {
//            lastUpdate = now;
//            proximiio.updatePositions(state.getGeoPoint().getLatitude(), state.getGeoPoint().getLongitude(), state.getUncertainty(), ProximiioInput.InputType.IndoorAtlas);
//        }
//    }


//    /**
//     * Since 21.4.2015 and Indoor Atlas 1.4, this means that the background calibration is complete,
//     * and that maximum accuracy is now available. Positioning CAN be started before this occurs.
//     */
//    @Override
//    public void onCalibrationReady() {
//        proximiio.toast("onCalibrationReady");
//        maxAccuracy = true;
//        if (listener != null) {
//            listener.indoorAtlasAccurate();
//        }
//    }


    @Override
    public void onLocationChanged(IALocation iaLocation) {
        Log.d(TAG, "IA LOCATION CHANGED" + iaLocation.toString());
        proximiio.toast("IA LOCATION CHANGED" + iaLocation.toString());
        long now = System.nanoTime();
        mLastLocation = iaLocation;
        if ((now - lastUpdate) / 1000000 > delay) {
            lastUpdate = now;
            proximiio.updatePositions(iaLocation.getLatitude(), iaLocation.getLongitude(), iaLocation.getAccuracy(), ProximiioInput.InputType.IndoorAtlas);
            proximiio.indoorAtlasInput(mLastLocation, false);
            proximiio.toast("UPDATE POSITIONS CALLED");
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        Log.d(TAG, "IA STATUS CHANGED" + s + " i:" + i + " bundle:" + (bundle == null ? "null" : bundle));
        if (i == IALocationManager.STATUS_AVAILABLE) {
            proximiio.toast("IA AVAILABLE");
        }
    }

}
