package com.navtureapps.proximiioandroid;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.webkit.WebView;

import java.util.ArrayList;

/**
 * Represents an input. Contains information about this input, as well as an action flow to hold the output.
 */
public class ProximiioInput {
    private static final String TAG = "ProximiioInput";

    private ProximiioBeacon beacon;
    private ProximiioCoordinates coordinates;
    private double accuracy;
    private InputType type;
    private boolean entered;
    private String id;
    private String departmentID;
    private String visitorID;
    private ProximiioActionFlow actionFlow;
    private ProximiioDepartment department;
    private String name;
    private String floorChange;
    private String venueChange;

    /**
     * Possible input types.
     */
    public enum InputType {
        /**
         * A iBeacon.
         */
        iBeacon,

        /**
         * An Indoor Atlas geofence.
         */
        IndoorAtlas,

        /**
         * Native geofence.
         */
        GPSGeofence,

        /**
         * SteerPath geofence.
         */
        SteerPath,

        /**
         * EddyStone geofence.
         */
        EddyStone
    }

    /**
     * Longitude of this input in WGS84.
     * @return longitude
     */
    public double getLon() {
        return coordinates.getLon();
    }

    /**
     * Latitude of this input in WGS84.
     * @return latitude
     */
    public double getLat() {
        return coordinates.getLat();
    }

    /**
     * Radius of this input in meters.
     * @return radius
     */
    public double getRadius() {
        return coordinates.getRadius();
    }

    /**
     * Accuracy of this input event. With iBeacons, this is the accuracy value reported, aka. the distance to iBeacon.
     * With other input types, this is the accuracy of the location estimate. Values in meters.
     * @return accuracy
     */
    public double getAccuracy() {
        return accuracy;
    }

    /**
     * Get the input type of this input.
     * @return input type
     */
    public InputType getType() {
        return type;
    }

    /**
     * Get the ID of this input.
     * @return ID
     */
    public String getId() {
        return id;
    }

    protected void setDepartment(ProximiioDepartment department) {
        this.department = department;
    }

    protected ProximiioDepartment getDepartment() {
        return department;
    }

    /**
     * Gets the iBeacon associated with this input. Available if InputType of this input is iBeacon.
     * @return The iBeacon if found, otherwise null.
     */
    public ProximiioBeacon getBeacon() {
        return beacon;
    }

    /**
     * Return the associated department's ID.
     * @return ID
     */
    public String getDepartmentID() {
        return departmentID;
    }

    /**
     * Did this event occur when entering or leaving an input?
     * @return entered
     */
    public boolean getEntered() {
        return entered;
    }

    protected void setEntered(boolean entered) {
        this.entered = entered;
    }

    protected void setAccuracy(double accuracy) {
        this.accuracy = accuracy;
    }

    public String getVisitorID() {
        return visitorID;
    }

    protected void setVisitorID(String id) {
        visitorID = id;
    }

    protected void setActionFlow(ProximiioActionFlow actionFlow) {
        this.actionFlow = actionFlow;
    }

    /**
     * Gets the action flow associated with this input.
     * @return Action flow if found, otherwise null.
     */
    public ProximiioActionFlow getActionFlow() {
        return actionFlow;
    }

    /**
     * Get the name of this input.
     * @return name
     */
    public String getName() {
        return name;
    }

    protected void setFloorChange(String floorID) {
        floorChange = floorID;
    }

    protected String getFloorChange() {
        return floorChange;
    }

    protected void setVenueChange(String venueID) {
        venueChange = venueID;
    }

    protected String getVenueChange() {
        return venueChange;
    }

//    protected ProximiioInput(ProximiioBeacon iBeacon, double lat, double lon, String id, String departmentID, String name) {
////        init(iBeacon, lat, lon, 0, InputType.iBeacon, id, departmentID, name);
//    }

    protected ProximiioInput(ProximiioBeacon beacon, double lat, double lon, String id, String departmentID, String name) {
        if (beacon.beaconType == ProximiioBeacon.BEACON_TYPE_IBEACON) {
            init(beacon, lat, lon, 0, InputType.iBeacon, id, departmentID, name);
        } else if (beacon.beaconType == ProximiioBeacon.BEACON_TYPE_EDDYSTONE) {
            init(beacon, lat, lon, 0, InputType.EddyStone, id, departmentID, name);
        }

    }

    protected ProximiioInput(double lat, double lon, double radius, InputType type, String id, String departmentID, String name) {
        init(null, lat, lon, radius, type, id, departmentID, name);
    }

    private void init(ProximiioBeacon beacon, double lat, double lon, double radius, InputType type, String id, String departmentID, String name) {
        this.beacon = beacon;
        this.coordinates = new ProximiioCoordinates(lat, lon, radius);
        this.type = type;
        this.id = id;
        this.departmentID = departmentID;
        this.name = name;
        entered = false;
    }

    protected boolean entered(double lat, double lon) {
        boolean chance = !entered && type == InputType.IndoorAtlas && coordinates.contains(lat, lon);
        if (chance) {
            entered = true;
            return true;
        }
        else {
            return false;
        }
    }

    protected boolean left(double lat, double lon) {
        boolean chance = entered && type == InputType.IndoorAtlas && !coordinates.contains(lat, lon);
        if (chance) {
            entered = false;
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Automatically handle output generated by this input.
     * You can manually access the output by calling {@link #getActionFlow()} and then calling {@link ProximiioActionFlow#getOutputs()} on that.
     * @param activity Your activity.
     * @param webView The WebView to display web content in. Can be null if xhtml is false.
     * @param push Do you want to generate push messages?
     * @param xhtml Do you want to generate xhtml content?
     */
    public void handleOutput(Activity activity, final WebView webView, boolean push, boolean xhtml) {
        if (actionFlow != null) {
            ArrayList<ProximiioOutput> output = actionFlow.getOutputs();
            for (int i = 0; i < output.size(); i++) {
                final ProximiioOutput out = output.get(i);
                switch (out.getType()) {
                    case Push:
                        if (push) {
                            notification(activity, out.getTitle(), out.getContent());
                        }
                        break;
                    case XHTML:
                        if (xhtml && webView != null) {
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    webView.loadData(out.getContent(), "text/html; charset=UTF-8", null);
                                }
                            });
                        }
                        break;
                }
            }
        }
    }

    private void notification(Activity activity, String title, String content) {
        Intent notificationIntent = new Intent(activity, activity.getClass());
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent contentIntent = PendingIntent.getActivity(activity, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        NotificationManager nm = (NotificationManager)activity.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification.Builder builder = new Notification.Builder(activity);

        builder.setContentIntent(contentIntent)
                .setSmallIcon(R.drawable.navture512)
                .setContentTitle(title)
                .setContentText(content);

        Notification n = builder.build();
        n.flags |= Notification.FLAG_AUTO_CANCEL;

        nm.notify(1, n);
    }

    public static String getInputTypeString(InputType type) {
        switch (type) {
            case iBeacon:
                return "iBeacon"; // iBeacon / EddyStone
            case IndoorAtlas:
                return "IndoorAtlas Geofences";
            case GPSGeofence:
                return "GPS Geofence";
            case SteerPath:
                return "SteerPath";
            case EddyStone:
                return "Eddystone Beacon";
            default:
                return "Unknown";
        }
    }
}
