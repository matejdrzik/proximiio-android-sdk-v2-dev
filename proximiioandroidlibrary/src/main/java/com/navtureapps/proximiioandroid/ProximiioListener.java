package com.navtureapps.proximiioandroid;

/**
 * The listener that's given to Proximiio to receive information.
 */
public abstract class ProximiioListener {

    /**
     * Possible failure states.
     */
    public enum Error {

        /**
         * The appID specified for Proximiio was invalid.
         */
        InvalidAppID,

        /**
         * The application found was not of mobile type.
         */
        NotMobile,

        /**
         * This instance of Proximiio has been merged to a newer instance.
         */
        Merged,

        /**
         * The token specified was invalid.
         */
        InvalidToken
    }

    /**
     * Triggered when an event is triggered with the flow "enter", aka an input is entered.
     * @param input The input that triggered the event.
     */
    public abstract void eventEnter(ProximiioInput input);

    /**
     * Triggered when an event is triggered with the flow "leave", aka an input is left.
     * @param input The input that triggered the event.
     */
    public abstract void eventLeave(ProximiioInput input);

    /**
     * Triggered if an error occurs.
     * @param error The error that occured.
     */
    public abstract void error(Error error);

    /**
     * Triggered when scan status has changed for an input type,
     * and on {@link com.navtureapps.proximiioandroid.Proximiio#setListener(ProximiioListener)}.
     * Note that this may result if Proximiio is moved to background
     * and a scan type has been enabled for foreground but not background operation (or vice versa).
     * @param inputType The type that is affected.
     * @param active Was the scanning turned on or off?
     */

    public void scanChange(ProximiioInput.InputType inputType, boolean active) {

    }

    /**
     * Triggered when a new position is available for any type.
     * @param lat Latitude in WSG84.
     * @param lon Longitude in WSG84.
     * @param accuracy Accuracy in meters.
     * @param inputType The input type that triggered this.
     */
    public void positionChange(double lat, double lon, double accuracy, ProximiioInput.InputType inputType) {

    }

    /**
     * Triggered when Indoor Atlas has reached maximum accuracy,
     * and on {@link com.navtureapps.proximiioandroid.Proximiio#setListener(ProximiioListener)} if Indoor Atlas is on max accuracy.
     */
    public void indoorAtlasAccurate() {

    }

    /**
     * Triggered when Indoor Atlas API keys have changed,
     * and on {@link com.navtureapps.proximiioandroid.Proximiio#setListener(ProximiioListener)}
     * @param organizationID Your organization ID.
     * @param anonymousID Your device ID.
     * @param indoorAtlasApiKey Indoor Atlas Api Key.
     * @param indoorAtlasApiSecret Indoor Atlas Api Secret.
     */
    public void apiKeyChange(String organizationID, String anonymousID, String indoorAtlasApiKey, String indoorAtlasApiSecret) {

    }

    /**
     * Triggered when Proximiio is ready to move to background. It's recommended to first call stopActivity,
     * and then wait for this before you finish/destroy the Activity. This is not needed between Activity transitions.
     */
    public void safeBackground() {

    }
}
