//Copyright � 2014 NavtureApps (http://navtureapps.com/)

package com.navtureapps.proximiioandroid;

import org.json.JSONObject;

public abstract class ProximiioNetReceiver extends ProximiioFiltered {

	private final String tag = "ProximiioNetReceiver";

	/**
	 * Called when the network handler encounters an error.
	 * 
	 * @param e
	 *            The Exception raised.
	 * @param beacon
	 *            The iBeacon that was being handled.
	 */
	public abstract void error(Exception e, ProximiioBeacon beacon);

	/**
	 * Called on a succesful data retrieval from server.
	 * 
	 * @param json
	 *            The data returned.
	 * @param beacon
	 *            The iBeacon that this data belongs to.
	 */
	public abstract void success(JSONObject json, ProximiioBeacon beacon);

	@Override
	protected String getTag() {
		return tag;
	}
}