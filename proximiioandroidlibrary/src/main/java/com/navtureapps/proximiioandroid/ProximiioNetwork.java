//Copyright � 2014 NavtureApps (http://navtureapps.com/)

package com.navtureapps.proximiioandroid;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.AbstractHttpMessage;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.util.Log;

import com.google.android.gms.location.Geofence;

public class ProximiioNetwork {
	private static String eventsURL = "http://api.proximi.io/events/register";
	private static String geofenceURL = "http://api.proximi.io/geofences/list";
	private ArrayList<ProximiioNetReceiver> netReceivers;
	private HashMap<ProximiioBeacon, JSONObject> cached;
	private HttpClient httpclient;
	private String apiKey;

	private enum EventType {
		SUCCESS, ERROR
	}

	/**
	 * Create a new instance of this class. Please note that this is
	 * automatically done when creating a ProximiioAPI instance. You should only
	 * create your own instance if you aren't using ProximiioAPI and want to
	 * communicate with the Proximiio REST API.
	 */
	public ProximiioNetwork(String apiKey) {
		netReceivers = new ArrayList<ProximiioNetReceiver>();
		cached = new HashMap<ProximiioBeacon, JSONObject>();
		httpclient = new DefaultHttpClient();
		this.apiKey = apiKey;
	}

	/**
	 * Add a receiver.
	 * 
	 * @param receiver
	 *            The receiver to add.
	 */
	public void addReceiver(ProximiioNetReceiver receiver) {
		netReceivers.add(receiver);
		Iterator<Entry<ProximiioBeacon, JSONObject>> pairs = cached.entrySet().iterator();
		while (pairs.hasNext()) {
			Entry<ProximiioBeacon, JSONObject> e = pairs.next();
			receiver.success(e.getValue(), e.getKey());
		}
	}

	/**
	 * Remove a receiver.
	 * 
	 * @param receiver
	 *            The receiver to remove.
	 */
	public void removeReceiver(ProximiioNetReceiver receiver) {
		netReceivers.remove(receiver);
	}

	/**
	 * Same as the overload, except that there's no ProximiioIBeacon specified.
	 * Instead, only UUID, minor, and major are supplied. The receivers will
	 * still receive an iBeacon constructed from these elements, so please note
	 * that they won't have any other reliable information.
	 * 
	 * @see #postBeaconStatus(Activity, String, ProximiioBeacon, boolean)
	 * @param apiKey
	 *            The API key to use when connecting to Proximiio.
	 * @param UUID
	 *            UUID of the iBeacon.
	 * @param major
	 *            Major of the iBeacon.
	 * @param minor
	 *            Minor of the iBeacon.
	 * @param entered
	 *            true for found, false for lost.
	 */
	public void postBeaconStatus(Activity activity, String UUID, int major, int minor, final boolean entered) {
		postBeaconStatus(activity, new ProximiioBeacon(UUID, major, minor), entered);
	}

	/**
	 * Post status. Handled automatically by ProximiioAPI instances. However, if
	 * you're not using any, this should be called at every lost and found
	 * event.
	 * 
	 * @param apiKey
	 *            The API key to use when connecting to Proximiio.
	 * @param beacon
	 *            The iBeacon lost or found.
	 * @param entered
	 *            true for found, false for lost.
	 */
	public void postBeaconStatus(Activity activity,
			final ProximiioBeacon beacon, final boolean entered) {
		Log.d("ProximiioNetwork", "post beacon status to network");
		ArrayList<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
		params.add(new BasicNameValuePair("input_type", "beacon"));
		params.add(new BasicNameValuePair("device_uuid", ProximiioIdentifier.getID(activity)));
		params.add(new BasicNameValuePair("uuid", beacon.getUUID()));
		params.add(new BasicNameValuePair("major", String.valueOf(beacon.getMajor())));
		params.add(new BasicNameValuePair("minor", String.valueOf(beacon.getMinor())));
		params.add(new BasicNameValuePair("arrived", entered ? "1" : "0"));

		int userInfoCount = addInfo(ProximiioIdentifier.getUserInfoIterator(activity), params, "user");
		int eventInfoCount = addInfo(ProximiioIdentifier.getEventInfoIterator(activity), params, "event_info");
		if (userInfoCount != 0 && eventInfoCount == 0) {
			params.add(new BasicNameValuePair("event_info[placeholder]", "mandatory"));
		}

		final HttpPost httpPost = new HttpPost(eventsURL);
		addHeaders(httpPost);

		if (params != null) {
			try {
				httpPost.setEntity(new UrlEncodedFormEntity(params));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}

		if (!entered) {
			cached.remove(beacon);
		}

		new Thread() {
			public void run() {
				ResponseHandler<String> responseHandler = new BasicResponseHandler();
				try {
					String message = httpclient.execute(httpPost, responseHandler);
					try {
						JSONObject json = new JSONObject(message);

						netReceiverEvent(EventType.SUCCESS, json, beacon);
						if (entered) {
							cached.put(beacon, json);
						}
					} catch (JSONException e) {
						onError(e, beacon);
					}
				} catch (ClientProtocolException e) {
					onError(e, beacon);
				} catch (IOException e) {
					onError(e, beacon);
				}
			}
		}.start();
	}

	public void getGeofences(final ProximiioGeofencing geofencing) {
		final HttpGet httpGet = new HttpGet(geofenceURL);
		// addHeaders(httpGet);
		// Use a custom api key for testing.
		httpGet.addHeader("X-API-KEY",
				"6265ed947cebe9cff284d0fa90fe648ebe68d599");
		httpGet.addHeader("Content-Type", "application/x-www-form-urlencoded");
		httpGet.addHeader("Accept", "application/json");

		new Thread() {
			public void run() {
				ResponseHandler<String> responseHandler = new BasicResponseHandler();
				try {
					String message = httpclient.execute(httpGet, responseHandler);
					try {
						populateGeofences(new JSONArray(message), geofencing);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}.start();
	}

	private void addHeaders(AbstractHttpMessage client) {
		client.addHeader("X-API-KEY", apiKey);
		client.addHeader("Content-Type", "application/x-www-form-urlencoded");
		client.addHeader("Accept", "application/json");
	}

	private void populateGeofences(JSONArray json,
			ProximiioGeofencing geofencing) {
		for (int i = 0; i < json.length(); i++) {
			try {
				JSONObject entry = json.getJSONObject(i);
				Geofence geofence = new Geofence.Builder()
						.setCircularRegion(
								Double.parseDouble(entry.getString("lat")),
								Double.parseDouble(entry.getString("long")),
								//Float.parseFloat(entry.getString("radius")))
								//60.3998523,
								//25.0807877,
								Float.MAX_VALUE)
						.setExpirationDuration(Geofence.NEVER_EXPIRE)
						.setRequestId(entry.getString("id"))
						.setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
						.build();
				geofencing.addGeofence(geofence);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	private int addInfo(Iterator<Entry<String, String>> iter, ArrayList<BasicNameValuePair> params, String arrayName) {
		int counter = 0;
		while (iter.hasNext()) {
			Entry<String, String> entry = iter.next();
			String value = entry.getValue();
			if (!value.equals("")) {
				params.add(new BasicNameValuePair(arrayName + "[" + entry.getKey() + "]", value));
				counter++;
			}
		}
		return counter;
	}

	private void onError(Exception e, ProximiioBeacon beacon) {
		e.printStackTrace();
		netReceiverEvent(EventType.ERROR, e, beacon);
	}

	private void netReceiverEvent(EventType type, Object argument, ProximiioBeacon beacon) {
		for (int i = 0; i < netReceivers.size(); i++) {
			ProximiioNetReceiver netReceiver = netReceivers.get(i);
			if (netReceiver.validBeacon(beacon)) {
				switch (type) {
				case SUCCESS:
					netReceiver.success((JSONObject)argument, beacon);
					break;
				case ERROR:
					netReceiver.error((Exception)argument, beacon);
					break;
				}
			}
		}
	}
}