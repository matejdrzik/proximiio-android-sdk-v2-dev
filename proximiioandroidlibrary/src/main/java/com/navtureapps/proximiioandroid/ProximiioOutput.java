package com.navtureapps.proximiioandroid;

/**
 * A single output. An input may contain several of these via an action flow.
 */
public class ProximiioOutput {
    private String id;
    private String content;
    private String title;
    private OutputType type;

    /**
     * Get this output's ID.
     * @return id
     */
    public String getID() {
        return id;
    }

    /**
     * Get the type of this output.
     * @return type
     */
    public OutputType getType() {
        return type;
    }

    /**
     * Get the title of this output.
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Get the content of this output.
     * @return content
     */
    public String getContent() {
        return content;
    }

    /**
     * Different possible output types.
     */
    public enum OutputType {

        /**
         * Output for a push message.
         */
        Push,

        /**
         * Output for a WebView.
         */
        XHTML,

        /**
         * Output for a REST API, not intended to be handled on mobile.
         */
        REST,

        /**
         * Custom output.
         */
        Raw
    }

    protected ProximiioOutput(String id, String content, String title, String type) {
        this.id = id;
        this.content = content;
        this.title = title;
        if (type.equals(getOutputTypeString(OutputType.Push))) {
            this.type = OutputType.Push;
        }
        else if (type.equals(getOutputTypeString(OutputType.XHTML))) {
            this.type = OutputType.XHTML;
        }
        else if (type.equals(getOutputTypeString(OutputType.Raw))) {
            this.type = OutputType.Raw;
        }
        else {
            this.type = OutputType.REST;
        }
    }

    protected static String getOutputTypeString(OutputType type) {
        switch (type) {
            case Push:
                return "Push Messages";
            case REST:
                return "REST API";
            case XHTML:
                return "XHTML";
            case Raw:
                return "Raw Data";
            default:
                return "Unknown";
        }
    }
}
