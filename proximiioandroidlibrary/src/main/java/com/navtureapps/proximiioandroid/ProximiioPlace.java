package com.navtureapps.proximiioandroid;

public class ProximiioPlace {
    private String name;
    private String id;
    private String indoorAtlasVenueID;

    protected String getName() {
        return name;
    }

    protected String getID() {
        return id;
    }

    protected String getIndoorAtlasVenueID() {
        return indoorAtlasVenueID;
    }

    public ProximiioPlace(String id, String name, String indoorAtlasVenueID) {
        this.name = name;
        this.id = id;
        this.indoorAtlasVenueID = indoorAtlasVenueID;
    }
}
