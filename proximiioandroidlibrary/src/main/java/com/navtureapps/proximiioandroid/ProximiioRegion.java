//Copyright � 2014 NavtureApps (http://navtureapps.com/)

package com.navtureapps.proximiioandroid;

import android.util.Log;

public class ProximiioRegion {
    public static final String TAG = "ProximiioRegion";
	private int type;
	private String uuid;
	private boolean useMajor;
	private int major;
	private boolean useMinor;
	private int minor;
	private String namespace;
	private boolean useNamespace;
	private String instanceId;
	private boolean useInstanceId;
	private boolean exclude;

	/**
	 * Is this region filter in exclude mode?
	 * 
	 * @return true if this region only accepts those iBeacons that don't
	 *         correspond to its' values.
	 */
	public boolean isExclude() {
		return exclude;
	}

	/**
	 * Make this region only accept iBeacons that don't correspond to this
	 * region's values (true parameter), or make it accept iBeacons that do
	 * correspond to its' values (false parameter).
	 * 
	 * @param exclude
	 *            Exclude mode on/off
	 */
	public void setExclude(boolean exclude) {
		this.exclude = exclude;
	}

	public String getUUID() {
		return uuid;
	}

	public int getType() { return type; }

	public int getMajor() {
		return major;
	}

	public int getMinor() {
		return minor;
	}

	public boolean hasMajor() {
		return useMajor;
	}

	public boolean hasMinor() {
		return useMinor;
	}

	public String getNamespace() { return namespace; }

	public String getInstanceId() { return instanceId; }

	public boolean hasNamespace() { return useNamespace; }

	public boolean hasInstanceId() { return useInstanceId; }

	//	Create eddystone compatible region filter
	public ProximiioRegion(ProximiioBeacon beacon) {
        this.type = beacon.beaconType;
		if (beacon.beaconType == ProximiioBeacon.BEACON_TYPE_IBEACON) {
			this.uuid = beacon.getUUID();
			this.major = beacon.getMajor();
			this.minor = beacon.getMinor();
			useMajor = true;
			useMinor = true;
		} else if (beacon.beaconType == ProximiioBeacon.BEACON_TYPE_EDDYSTONE) {
			this.namespace = beacon.getNamespace();
			this.instanceId = beacon.getInstanceId();
			useNamespace = true;
			useInstanceId = true;
		}
        exclude = false;
	}
	/**
	 * Create a region filter. This filter will only accept those iBeacons that
	 * correspond (or don't, according to setExclude) to all three values: UUID,
	 * major and minor. If you want to include all majors/minors of a UUID (or
	 * all minors of a major), use constructors that require only those
	 * arguments you need to filter by. You can reverse the filter's
	 * functionality with setExclude(true) function, which makes this filter
	 * only accept those iBeacons that don't correspond to this region.
	 * 
	 * @param uuid
	 *            The UUID for the filter.
	 * @param major
	 *            The major value.
	 * @param minor
	 *            The minor value.
	 */
	public ProximiioRegion(String uuid, int major, int minor) {
		this.uuid = uuid;
		this.major = major;
		this.minor = minor;
		useMajor = true;
		useMinor = true;
		exclude = false;
	}

	/**
	 * Create a region filter. iBeacons must pass both UUID and major.
	 * 
	 * @param uuid
	 *            The UUID for the filter.
	 * @param major
	 *            The major value.
	 * @see ProximiioRegion#ProximiioRegion(String,
	 *      int, int) ProximiioRegion(String, int, int)
	 */
	public ProximiioRegion(String uuid, int major) {
		this.uuid = uuid;
		this.major = major;
		useMajor = true;
		useMinor = false;
		exclude = false;
	}

	/**
	 * Create a region filter. iBeacons only need the same UUID.
	 * 
	 * @param uuid
	 *            The UUID for the filter.
	 * @see ProximiioRegion#ProximiioRegion(String,
	 *      int, int) ProximiioRegion(String, int, int)
	 */
	public ProximiioRegion(String uuid) {
		this.uuid = uuid;
		useMajor = false;
		useMinor = false;
		exclude = false;
	}

	/**
	 * Two regions are considered the same if their major, minor, and UUID are
	 * the same.
	 */

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ProximiioRegion)) {
			return false;
		}
		ProximiioRegion thatRegion = (ProximiioRegion) o;
		if (getType() == thatRegion.getType()) {
			if (getType() == ProximiioBeacon.BEACON_TYPE_IBEACON && thatRegion.getType() == ProximiioBeacon.BEACON_TYPE_IBEACON) {
				return thatRegion.getMajor() == major && thatRegion.getMinor() == minor
						&& thatRegion.getUUID() != null && thatRegion.getUUID().equals(uuid);
			} else if (type == ProximiioBeacon.BEACON_TYPE_EDDYSTONE) {
				return thatRegion.getNamespace().toLowerCase().equals(getNamespace().toLowerCase())
						&& thatRegion.getInstanceId().toLowerCase().equals(getInstanceId().toLowerCase());

			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	protected boolean isInRange(ProximiioBeacon beacon) {
        if (getType() == beacon.beaconType) {
            if (getType() == ProximiioBeacon.BEACON_TYPE_IBEACON) {
				if (useMajor) {
					if (beacon.getMajor() != major) {
						return false;
					} else if (useMinor && beacon.getMinor() != minor) {
						return false;
					}
				}
			}  else if (getType() == ProximiioBeacon.BEACON_TYPE_EDDYSTONE) {
				if (beacon.getNamespace() == null || beacon.getInstanceId() == null || namespace == null || instanceId == null) {
					return false;
				} else {
					return beacon.getNamespace().toLowerCase().equals(namespace.toLowerCase())
							&& beacon.getInstanceId().toLowerCase().equals(instanceId.toLowerCase());
				}
            } else {
                return false;
            }
        }
        return true;
    }

}