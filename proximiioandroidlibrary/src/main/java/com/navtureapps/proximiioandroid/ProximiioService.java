//Copyright � 2014 NavtureApps (http://navtureapps.com/)

package com.navtureapps.proximiioandroid;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothAdapter.LeScanCallback;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

public class ProximiioService extends Service {
    private static final String TAG = "ProximiioService";

	private Handler handler;
	private BluetoothAdapter blAdapter;
	private LeScanCallback scanCallback;
	private Runnable scanRunnable;
	private Runnable checkBluetoothRunnable;
	private Runnable checkLostIBeacons;
	private ArrayList<ProximiioBeacon> beacons;
	private ArrayList<ProximiioBeaconReceiver> clients;
	private String tag;
	private boolean configuring;
	private long serviceTimeoutTimer;
	private long previousTimestamp;

	private static ProximiioService instance;
	private static boolean enabled; // Use this to check if this service is
									// still wanted to be run.
	private static boolean scanning; // Use this to check if we're scanning at
										// the moment.

	private static int defaultScanDuration = 1500;
	private static int defaultScanInterval = 1500;
	private static int defaultIBeaconTimeout = 30000;
	private static int defaultIBeaconTimeoutInterval = 5000;
	private static int defaultIBeaconValid = 1500;

	/**
	 * The amount of time in milliseconds that a single Bluetooth LE scan will
	 * run for.
	 */
	public static int scanDuration = defaultScanDuration;

	/**
	 * Time in millis that will be waited before starting another scan.
	 * Important for devices which have WiFi and Bluetooth on the same
	 * radio/chip/whatever, meaning they can't be used simultaneously.
	 */
	public static int scanInterval = defaultScanInterval;

	/**
	 * Time in millis after an iBeacon is considered lost. Make sure this is at
	 * least double as big as scanDuration + scanInterval.
	 */
	public static int iBeaconTimeout = defaultIBeaconTimeout;

	/**
	 * Time in millis that will be waited in between checking for lost iBeacons.
	 * Should be shorter than iBeaconTimeout, because in the worst case scenario
	 * an iBeacon will become lost after iBeaconTimeout +
	 * iBeaconTimeoutInterval.
	 */
	public static int iBeaconTimeoutInterval = defaultIBeaconTimeoutInterval;

	/**
	 * Time in millis which an iBeacon will be considered up-to-date, and won't
	 * be called updates on.
	 */
	public static int iBeaconValid = defaultIBeaconValid;

	/**
	 * Time in millis that will be waited in between checking if bluetooth is
	 * turned on at the begirenning of service.
	 */
	public static int bluetoothCheckInterval = 5000;

	/**
	 * Time in millis after the service will be stopped if it doesn't have any
	 * receivers registered. Please note that this will be ignored when the last
	 * receiver gets unregistered.
	 */
	public static long serviceTimeout = 5000;
	private final Messenger messenger;

	private static enum eventType
	{
		FOUND,  UPDATED,  LOST,  REUPDATED;

		private eventType() {}
	}

	public static final int REGISTER_CLIENT = 1;
	public static final int UNREGISTER_CLIENT = 2;
	public static final int START_SCAN = 3;
	public static final int STOP_SCAN = 4;
	public static final int START_CONFIG = 7;
	public static final int END_CONFIG = 8;

	public ProximiioService()
	{
		this.messenger = new Messenger(new IncomingHandler(this));
	}

	private static class IncomingHandler
			extends Handler
	{
		private final WeakReference<ProximiioService> proximiioService;

		IncomingHandler(ProximiioService proximiioService)
		{
			this.proximiioService = new WeakReference(proximiioService);
		}

		public void handleMessage(Message msg)
		{
			ProximiioService service = this.proximiioService.get();
			switch (msg.what)
			{
				case REGISTER_CLIENT:
					ProximiioBeaconReceiver receiver = (ProximiioBeaconReceiver)msg.obj;
					if (!service.getExistingClient(receiver))
					{
						service.clients.add(receiver);
//                        Log.d(TAG, "service beacons size: " + service.beacons.size());
						for (int i = 0; i < service.beacons.size(); i++) {
							service.iBeaconActionForReceiver(receiver,
                                    (ProximiioBeacon) service.beacons.get(i), ProximiioService.eventType.FOUND, ProximiioBeaconReceiver.LostReason.TIMED_OUT);
//                            Log.d(TAG, "handle beacon found: " + service.beacons.get(i));
						}
//						Log.d(service.tag, "Receiver registered.");
					}
					else
					{
						Log.w(service.tag, "Receiver already registered!");
					}
					break;
				case UNREGISTER_CLIENT:
					ProximiioBeaconReceiver receiver2 = (ProximiioBeaconReceiver)msg.obj;
					if (service.clients.remove(receiver2))
					{
						service.triggerLostIBeacons(ProximiioBeaconReceiver.LostReason.RECEIVER_UNREGISTERED, receiver2);
//						Log.d(service.tag, "Receiver unregistered.");
					}
					else
					{
						Log.w(service.tag, "Receiver was not registered when it was passed to be unregistered!");
					}
					break;
				case START_SCAN:
					ProximiioService.scanning = true;
					service.startScans();
					Log.d(service.tag, "Scan started again.");
					break;
				case STOP_SCAN:
					ProximiioService.scanning = false;
					service.lostAllIBeacons(ProximiioBeaconReceiver.LostReason.SCAN_STOP);
					Log.d(service.tag, "Scan stopped.");
					break;
				case START_CONFIG:
					service.lostAllIBeacons(ProximiioBeaconReceiver.LostReason.CONFIGURING);
					service.configuring = true;
					break;
				case END_CONFIG:
					service.configuring = false;
					break;
				case 5:
				case 6:
				default:
					super.handleMessage(msg);
			}
		}
	}

	public IBinder onBind(Intent intent)
	{
		return this.messenger.getBinder();
	}

	public void onCreate()
	{
		instance = this;
		enabled = true;
		scanning = false;
		this.configuring = false;
		this.tag = "ProximiioService";
		this.serviceTimeoutTimer = 0L;
		this.previousTimestamp = System.nanoTime();
		this.handler = new Handler();
		this.blAdapter = BluetoothAdapter.getDefaultAdapter();
		this.beacons = new ArrayList();
		this.clients = new ArrayList();

		this.scanCallback = new BluetoothAdapter.LeScanCallback()
		{
			public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord)
			{
				if (clients.isEmpty())
				{
					long newTimeStamp = System.nanoTime();
					serviceTimeoutTimer = (serviceTimeoutTimer + (newTimeStamp - previousTimestamp) / 1000000L);
					previousTimestamp = newTimeStamp;
					if (serviceTimeoutTimer >= ProximiioService.serviceTimeout)
					{
						Log.d(tag, "No receivers, shutting down service.");
						stopService();
					}
				}
				else if ((ProximiioService.enabled) && (ProximiioService.scanning) && (!configuring))
				{
					serviceTimeoutTimer = 0L;
					previousTimestamp = System.nanoTime();

					int i;
					ProximiioBeacon existingBeacon = null;
					for (i = 0; i < beacons.size(); i++) {
						ProximiioBeacon testBeacon = beacons.get(i);
						if (testBeacon.mac.equals(device.getAddress())) {
							existingBeacon = beacons.get(i);
							break;
						}
					}

					ProximiioBeacon beacon = ProximiioBeacon.fromScanData(scanRecord, rssi, device, existingBeacon);
					if (beacon != null) {
						if (existingBeacon != null)  {
                            if (existingBeacon.beaconType == ProximiioBeacon.BEACON_TYPE_EDDYSTONE) {
                                if (existingBeacon.hasTelemetry() && !beacon.hasTelemetry()) {
                                    // existing beacon was telemetry, new one is uid/url
                                    existingBeacon.update(beacon);
                                    existingBeacon.namespace = beacon.namespace;
                                    existingBeacon.instanceId = beacon.instanceId;
                                } else {
                                    // existing beacon has no telemetry, but new one has
                                    existingBeacon.update(beacon);
                                }
                            }

							if (existingBeacon.millisecondsAgo() > ProximiioService.iBeaconValid)
							{
								existingBeacon.updateTime();
								if (!iBeaconAction(existingBeacon, ProximiioService.eventType.UPDATED)) {
									beacons.remove(i);
								}
							}
							else if (!iBeaconAction(existingBeacon, ProximiioService.eventType.REUPDATED))
							{
//								beacons.remove(i);
							}
						} else if (iBeaconAction(beacon, ProximiioService.eventType.FOUND))  {
							beacons.add(beacon);
						}
					}
				}
			}
		};
		this.scanRunnable = new Runnable()
		{
			public void run()
			{
				blAdapter.stopLeScan(scanCallback);
				if ((ProximiioService.enabled) && (ProximiioService.scanning)) {
					handler.postDelayed(new Runnable() {
						public void run() {
							runBluetoothScan();
						}
					}, ProximiioService.scanInterval);
				}
			}
		};
		this.checkBluetoothRunnable = new Runnable()
		{
			public void run()
			{
				checkBluetooth();
			}
		};
		this.checkLostIBeacons = new Runnable()
		{
			public void run()
			{
				if ((ProximiioService.enabled) && (ProximiioService.scanning))
				{
					for (int i = 0; i < beacons.size(); i++)
					{
						ProximiioBeacon beacon = beacons.get(i);
                        int beaconTimeout = beacon.beaconType == ProximiioBeacon.BEACON_TYPE_IBEACON ? ProximiioService.iBeaconTimeout : 25000;
						if (beacon.realMillisecondsAgo() >= beaconTimeout)
						{
							iBeaconAction(beacon, ProximiioService.eventType.LOST);
                            if (beacons.contains(beacon)) {
                                beacons.remove(i);
                            }
						}
					}
					handler.postDelayed(this, ProximiioService.iBeaconTimeoutInterval);
				}
			}
		};
	}

	public int onStartCommand(Intent intent, int flags, int startId)
	{
		if (!scanning) {
			checkBluetooth();
		}
		return 1;
	}

	public void onDestroy()
	{
		if (enabled) {
			stopService();
		}
	}

	public static boolean enabled()
	{
		return enabled;
	}

	public static boolean scanning()
	{
		return scanning;
	}

	private void checkBluetooth()
	{
		if (this.blAdapter.getState() != 12)
		{
			this.handler.postDelayed(this.checkBluetoothRunnable, bluetoothCheckInterval);
		}
		else
		{
			scanning = true;
			startScans();
		}
	}

	private void runBluetoothScan()
	{
		if ((!this.configuring) &&
				(!this.blAdapter.startLeScan(this.scanCallback))) {
			Log.e(this.tag, "Error starting Bluetooth scan! isConfiguring:" + this.configuring + " blAdapter startScan stopping... scan");
            this.blAdapter.stopLeScan(this.scanCallback);

		}
		this.handler.postDelayed(this.scanRunnable, scanDuration);
	}

	private void startScans()
	{
		runBluetoothScan();
		this.handler.postDelayed(this.checkLostIBeacons, iBeaconTimeoutInterval);
	}

	private void lostAllIBeacons(ProximiioBeaconReceiver.LostReason reason)
	{
		for (int i = 0; i < this.beacons.size(); i++) {
			iBeaconAction(this.beacons.get(i), eventType.LOST, reason);
		}
		this.beacons.clear();
	}

	private void triggerLostIBeacons(ProximiioBeaconReceiver.LostReason reason, ProximiioBeaconReceiver receiver)
	{
		for (int i = 0; i < this.beacons.size(); i++) {
			iBeaconActionForReceiver(receiver, (ProximiioBeacon)this.beacons.get(i), eventType.LOST, reason);
		}
	}

	private void stopService()
	{
		this.blAdapter.stopLeScan(this.scanCallback);
		lostAllIBeacons(ProximiioBeaconReceiver.LostReason.RECEIVER_UNREGISTERED);
		enabled = false;
		scanning = false;
		stopSelf();
	}

	public static void destroy()
	{
		instance.stopService();
	}

	private boolean getExistingClient(ProximiioBeaconReceiver receiver)
	{
		for (int i = 0; i < this.clients.size(); i++) {

			if ((this.clients.get(i)).getReceiverID().equals(receiver.getReceiverID())) {
				return true;
			}
		}
		return false;
	}

	private boolean iBeaconAction(ProximiioBeacon beacon, eventType event)
	{
		return iBeaconAction(beacon, event, ProximiioBeaconReceiver.LostReason.TIMED_OUT);
	}

	private boolean iBeaconAction(ProximiioBeacon beacon, eventType event, ProximiioBeaconReceiver.LostReason reason)
	{
		boolean anyUse = false;
		for (int i = 0; i < this.clients.size(); i++) {
			if (iBeaconActionForReceiver((ProximiioBeaconReceiver)this.clients.get(i), beacon, event, reason)) {
				anyUse = true;
			}
		}
		return anyUse;
	}

	private boolean iBeaconActionForReceiver(ProximiioBeaconReceiver receiver, ProximiioBeacon beacon, eventType event, ProximiioBeaconReceiver.LostReason reason) {
        if (event == eventType.LOST) {
            receiver.lostBeacon(beacon, reason);
            return true;
        }

		if (receiver.validBeacon(beacon)) {
			switch (event) {
				case FOUND:
					receiver.foundBeacon(beacon);
					break;
				case UPDATED:
					receiver.updatedBeacon(beacon);
					break;
//				case LOST:
//					beacons.remove(beacon);
//					receiver.lostBeacon(beacon, reason);
//					break;
				case REUPDATED:
					receiver.updatedBeacon(beacon);
//					if (receiver.fastUpdate()) {
//						Log.d(TAG, "going thru fastUpdate");
//
//					}
					break;
			}
			return true;
		} else {
//            Log.d(TAG, "IS INVALID BEACON");
        }
		return false;
	}

}