package com.navtureapps.proximiioandroid;

import android.app.Activity;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

//import com.nimbledevices.indoorguide.GuideManager;
//import com.nimbledevices.indoorguide.ZoneListener;
//import com.nimbledevices.indoorguide.GuideManagerListener;

import java.io.File;

//public class ProximiioSteerPath implements GuideManagerListener, ZoneListener, LocationListener {
//    private GuideManager guideManager;
//    private Activity activity;
//    private Proximiio proximiio;
//    private int delay;
//    private long lastUpdate;
//
//    protected void setProximiio(Proximiio proximiio) {
//        this.proximiio = proximiio;
//    }
//
//    protected ProximiioSteerPath(Activity activity, String nddUrl, Proximiio proximiio) {
//        this.activity = activity;
//        this.proximiio = proximiio;
//        delay = 15000;
//        guideManager = GuideManager.getInstance(activity);
//        guideManager.addGuideManagerListener(this, activity);
//
//        newNDD(nddUrl);
//    }
//
//    protected void setNDD(String url) {
//        guideManager.unRequestLocationUpdates(this, activity);
//        guideManager.unRequestZoneUpdates(this, activity);
//        newNDD(url);
//    }
//
//    private void newNDD(String url) {
//        File nddTarget = new File(activity.getCacheDir(), "current.ndd");
//        guideManager.setNDD(url, nddTarget);
//    }
//
//    @Override
//    public void onError(Throwable e) {
//        e.printStackTrace();
//    }
//
//    @Override
//    public void onNDDLoaded() {
//        guideManager.requestLocationUpdates(this, activity);
//        guideManager.requestZoneUpdates(this, activity);
//    }
//
//    @Override
//    public void onNDDLoadFailure(Throwable e) {
//        e.printStackTrace();
//    }
//
//    @Override
//    public void onZoneEntered(long zoneId, String zoneName) {
//        toast("Entered " + zoneName);
//    }
//
//    @Override
//    public void onZoneExited(long zoneId, String zoneName) {
//        toast("Exited " + zoneName);
//    }
//
//    @Override
//    public void onLocationChanged(Location location) {
//        if (!Double.isNaN(location.getLatitude())) {
//            proximiio.steerPathInput(location.getLatitude(), location.getLongitude(), location.getAccuracy());
//            long now = System.nanoTime();
//            if ((now - lastUpdate) / 1000000 > delay) {
//                lastUpdate = now;
//                proximiio.updatePositions(location.getLatitude(), location.getLongitude(), location.getAccuracy(), ProximiioInput.InputType.SteerPath);
//            }
//        }
//        //toast("Location: " + String.valueOf(location.getLatitude()) + ", " + String.valueOf(location.getLongitude()));
//    }
//
//    @Override
//    public void onStatusChanged(String provider, int status, Bundle extras) {
//
//    }
//
//    @Override
//    public void onProviderEnabled(String provider) {
//
//    }
//
//    @Override
//    public void onProviderDisabled(String provider) {
//
//    }
//
//    private void toast(final String message) {
//        Log.d("ProximiioSteerPath", message);
//        activity.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
//            }
//        });
//    }
//
//    protected void destroy() {
//        guideManager.stopUpdates(activity);
//        try {
//            GuideManager.destroy();
//        }
//        catch (Throwable e) {
//            e.printStackTrace();
//        }
//        guideManager = null;
//    }
//}
